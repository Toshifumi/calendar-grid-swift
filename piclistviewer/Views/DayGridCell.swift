//
//  DayGridCell.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2020/12/18.
//

import SwiftUI

struct DayGridCell: View {
    
    var calendarID: Int
//    let dayItem: DayItem
    
    @ObservedObject var dayItem: DayItem
    @ObservedObject private var photoFetcher = PhotoFetcher()
    var appCalendarSetting:AppCalendarSetting

    init(calendarID: Int, dayItem:DayItem, appCalendarSetting:AppCalendarSetting) {
        self.calendarID = calendarID
        self.dayItem = dayItem
        self.appCalendarSetting = appCalendarSetting
        if self.dayItem.day != 0 && !self.dayItem.assetLoaded {
            self.photoFetcher.askDayItemToPhotoLibrary(calendarID: calendarID, dayItem: dayItem)
        }
    }
    
    
    var body: some View {
        
        ZStack {
            
            if dayItem.day == 0 {
                ZStack {
                    Color(.systemBackground)
                }
            } else {
                
                if (self.photoFetcher.taskCompleted || self.dayItem.assetLoaded) && self.dayItem.numberOfImages != 0 {
                    
                    NavigationLink(destination: DaysMainView( calendarID: self.calendarID, baseDayItem: dayItem, appCalendarSetting:appCalendarSetting)) {

                        ZStack {
                            
                            if dayItem.day == 0 {
                            } else {
                                Color(.systemGray5)
                            }
                            
                            GeometryReader { geometry in
                                Image(uiImage: self.dayItem.coverImage)
                                    .resizable()
                                    .scaledToFill()
                                    .frame(width: geometry.size.width, height: geometry.size.height)
                                    .clipped()
                            }
                            
                            VStack {
                                
                                HStack {
                                    
                                    StrokeText(text: String(format: "%d", dayItem.day), width: 0.8, color: .white)
                                        .foregroundColor(.black)
                                        .font(.system(size: 12, weight: .bold))
                                    
                                    if self.appCalendarSetting.showFixedColumns {

                                        StrokeText(text: getWeekOfDayText(), width: 0.8, color: .white)
                                            .foregroundColor(.black)
                                            .font(.system(size: 8, weight: .regular))
                                        
                                    }

                                    Spacer()
                                }
                                
                                Spacer()
                                
                                HStack {
                                    Spacer()
                                    
                                    if dayItem.numberOfImages != 0 {
                                        StrokeText(text: String(format: "%d", dayItem.numberOfImages), width: 0.8, color: .white)
                                            .foregroundColor(.black)
                                            .font(.system(size: 10, weight: .bold))
                                    }
                                }
                            }
                            .padding(.all, 4.0)
                        }
                    }

                    
                } else {

                    ZStack {
                        
                        if dayItem.day == 0 {
                        } else {
                            Color(.systemGray5)
                        }
                        
                        VStack {
                            
                            HStack {
                                StrokeText(text: String(format: "%d", dayItem.day), width: 0.8, color: .white)
                                    .foregroundColor(.black)
                                    .font(.system(size: 12, weight: .bold))
                                
                                if self.appCalendarSetting.showFixedColumns {

                                    StrokeText(text: getWeekOfDayText(), width: 0.8, color: .white)
                                        .foregroundColor(.black)
                                        .font(.system(size: 8, weight: .regular))
                                    
                                }

                                Spacer()
                            }
                            
                            Spacer()
                            
                        }
                        .padding(.all, 4.0)
                    }
                    
                }
            }
            
        }
        .onReceive(self.dayItem.$assetLoaded, perform: { _ in
            if !self.dayItem.assetLoaded {
                self.photoFetcher.askDayItemToPhotoLibrary(calendarID: calendarID, dayItem: dayItem)
            }
        })

    }
    

    func getWeekOfDayText() -> String {
        
        let dateFormatter = DateFormatter()
        let language = Locale.preferredLanguages.first!
        let locale = Locale(identifier: language)
        dateFormatter.locale = locale
        let symbols = dateFormatter.shortWeekdaySymbols
        
        return symbols![dayItem.weekOfDay-1]

    }
    
}


struct StrokeText: View {
    let text: String
    let width: CGFloat
    let color: Color
    
    var body: some View {
        ZStack{
            ZStack{
                Text(text).offset(x:  width, y:  width)
                Text(text).offset(x: -width, y: -width)
                Text(text).offset(x: -width, y:  width)
                Text(text).offset(x:  width, y: -width)
            }
            .foregroundColor(color)
            Text(text)
        }
    }
}


struct DayGridCell_Previews: PreviewProvider {
    static var previews: some View {
        
        DayGridCell(calendarID: 0,  dayItem: DayItem(), appCalendarSetting: AppCalendarSetting(calendarID: 0) )
            .previewLayout(.fixed(width: 100, height: 100))
        
    }
}
