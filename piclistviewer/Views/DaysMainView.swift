//
//  DaysMainView.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2020/12/25.
//

import SwiftUI

struct DaysMainView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var calendarID: Int
    let baseDayItem:DayItem
    var appCalendarSetting:AppCalendarSetting

    var body: some View {

        ZStack(alignment: .bottomTrailing) {
            DaysViewController(calendarID: self.calendarID, baseDayItem: self.baseDayItem, appCalendarSetting:appCalendarSetting)
        }
        .onAppear() {
            
            if !UserDefaults.standard.bool(forKey: "done_showInstruction_DayView") {
                RootViewManager.shared.showInstruction_DayView = true
                RootViewManager.shared.showInstruction_DayView_Trigger += 1
                UserDefaults.standard.set(true, forKey: "done_showInstruction_DayView")
            }
            
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: backButton)
        
    }
    
    
    var backButton : some View { Button(action: {
        self.presentationMode.wrappedValue.dismiss()
    }) {
        HStack {
            Image(systemName: "chevron.backward")
                .aspectRatio(contentMode: .fit)
            Text("Back") //translated Back button title
        }
    }
    }

}



struct DaysMainView_Previews: PreviewProvider {
    static var previews: some View {
        DaysMainView(calendarID: 0, baseDayItem: DayItem(year: 2020, month: 12, day: 1), appCalendarSetting:AppCalendarSetting(calendarID: 0))
    }
}
