//
//  ImageGridCell.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2020/12/29.
//

import SwiftUI

struct ImageGridCell: View {
    
    var calendarID: Int
    var dayItem: DayItem

    var items:Array<String>
    var index:Int
    var isCoverImage:Bool

    var imageItem:ImageItem

    @ObservedObject private var photoFetcher = PhotoFetcher()
    
    @State var allowToNavigation:Bool = false
    

    init(calendarID: Int, dayItem:DayItem, items:Array<String>, index:Int, isCoverImage:Bool) {
        
        self.calendarID = calendarID
        self.dayItem = dayItem
        
        self.items = items
        self.index = index
        self.imageItem = ImageItem()

        self.isCoverImage = isCoverImage

        self.imageItem.localIdentifier = items[index]
        self.photoFetcher.askImageItemToPhotoLibrary(imageItem: imageItem)

    }
    
    var body: some View {
        
        NavigationLink(destination: ImageMainView(calendarID: self.calendarID, dayItem:self.dayItem, items: self.items, currentIndex: self.index)  ) {
            
            ZStack {
                
                Color.gray
                
                GeometryReader { geometry in
                    Image(uiImage: self.imageItem.image)
                        .resizable()
                        .scaledToFill()
                        .frame(width: geometry.size.width, height: geometry.size.height)
                        .clipped()
                }
                
                if self.imageItem.isVideo {
                    Image(uiImage: UIImage(imageLiteralResourceName: "playButton"))
                        .resizable()
                        .frame(width: 48, height: 48)
                }
                
                VStack {
                    
                    Spacer()
                    
                    HStack {
                        Spacer()
                        
                        if isCoverImage {
                            Image(uiImage: UIImage(imageLiteralResourceName: "checke_On"))
                                .resizable()
                                .scaledToFill()
                                .frame(width: 24, height: 24)
                                .clipped()
                            
                        }
                        
                    }
                }
                .padding(.all, 4.0)
            }
            
        }
        
//        if self.dayItem.assetLoaded {
//
//            NavigationLink(destination: ImageMainView(calendarID: self.calendarID, dayItem:self.dayItem, items: self.items, currentIndex: self.index)  ) {
//
//                ZStack {
//
//                    Color.gray
//
//                    GeometryReader { geometry in
//                        Image(uiImage: self.imageItem.image)
//                            .resizable()
//                            .scaledToFill()
//                            .frame(width: geometry.size.width, height: geometry.size.height)
//                            .clipped()
//                    }
//
//                    if self.imageItem.isVideo {
//                        Image(uiImage: UIImage(imageLiteralResourceName: "playButton"))
//                            .resizable()
//                            .frame(width: 48, height: 48)
//                    }
//
//                    VStack {
//
//                        Spacer()
//
//                        HStack {
//                            Spacer()
//
//                            if isCoverImage {
//                                Image(uiImage: UIImage(imageLiteralResourceName: "checke_On"))
//                                    .resizable()
//                                    .scaledToFill()
//                                    .frame(width: 24, height: 24)
//                                    .clipped()
//
//                            }
//
//                        }
//                    }
//                    .padding(.all, 4.0)
//                }
//
//            }
//
//        } else {
//
//            ZStack {
//                Color.gray
//            }
//        }
        
        



    }
}

struct ImageGridCell_Previews: PreviewProvider {
    static var previews: some View {
        ImageGridCell(calendarID: 0, dayItem:DayItem(), items: [""], index: 0, isCoverImage: false)
            .previewLayout(.fixed(width: 100, height: 100))

    }
}
