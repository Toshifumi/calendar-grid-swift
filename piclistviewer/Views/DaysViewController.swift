//
//  DaysViewController.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2020/12/28.
//


import SwiftUI
import UIKit

struct DaysViewController: UIViewControllerRepresentable {

    var calendarID: Int
    var baseDayItem: DayItem
    var appCalendarSetting:AppCalendarSetting

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

    func makeUIViewController(context: Context) -> UIPageViewController {
        let pageViewController = UIPageViewController(
            transitionStyle: .scroll,
            navigationOrientation: .horizontal)
        pageViewController.dataSource = context.coordinator
        pageViewController.delegate = context.coordinator

        return pageViewController
    }

    func updateUIViewController(_ pageViewController: UIPageViewController, context: Context) {
        
        pageViewController.setViewControllers( [context.coordinator.currentDayView], direction: .forward, animated: true)
        
    }

    class Coordinator: NSObject, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
        
        let AssetMan = AssetManager.shared
        let localCalendar = Calendar.current
        var oldestDate_atCurrentCalendar:Date!
        var currentDate_atCurrentCalendar:Date!
        
        var currentDayItem:DayItem
        var beforeDayItem:DayItem!
        var afterDayItem:DayItem!

        var currentDayView:UIViewController
        var beforeDayView:UIViewController!
        var afterDayView:UIViewController!

        var parent: DaysViewController

        init(_ daysViewController: DaysViewController) {
            self.parent = daysViewController
            self.currentDayItem = parent.baseDayItem
            self.currentDayView = UIHostingController(rootView: DayView(calendarID: parent.calendarID, dayItem: currentDayItem))
            // print("first " , self.currentDayView.description) #1
            super.init()
            
            self.setLimitDates()
            self.calcBeforeDayItem(existingViewCon: nil)
            self.calcAfterDayItem(existingViewCon: nil)
        }

        func pageViewController(
            _ pageViewController: UIPageViewController,
            viewControllerBefore viewController: UIViewController) -> UIViewController? {

            return self.beforeDayView

        }

        func pageViewController(
            _ pageViewController: UIPageViewController,
            viewControllerAfter viewController: UIViewController) -> UIViewController? {

            return self.afterDayView
            
        }

        func pageViewController(
            _ pageViewController: UIPageViewController,
            didFinishAnimating finished: Bool,
            previousViewControllers: [UIViewController],
            transitionCompleted completed: Bool) {
            if completed {

                let visibleViewController = pageViewController.viewControllers?.first

                if visibleViewController == self.beforeDayView {
                    
                    self.currentDayItem = self.beforeDayItem
                    self.currentDayView = self.beforeDayView
                    self.calcBeforeDayItem(existingViewCon: nil)
                    self.calcAfterDayItem(existingViewCon: previousViewControllers.first)

                } else if visibleViewController == self.afterDayView {

                    self.currentDayItem = self.afterDayItem
                    self.currentDayView = self.afterDayView
                    self.calcBeforeDayItem(existingViewCon: previousViewControllers.first)
                    self.calcAfterDayItem(existingViewCon: nil)

                } else {
                    
                }
                
            }
        }
        
        func setLimitDates() {
            
            //add previous 10 days
            let oldestAssetDate = AssetMan.getOldestAssetDate()
            let oldestAssetDate_dateComponents = Calendar.current.dateComponents(in: TimeZone.current, from: oldestAssetDate )
            var oldestDate_dateComponents = DateComponents()
            oldestDate_dateComponents.year = oldestAssetDate_dateComponents.year
            oldestDate_dateComponents.month = oldestAssetDate_dateComponents.month
            oldestDate_dateComponents.day = oldestAssetDate_dateComponents.day
            self.oldestDate_atCurrentCalendar = localCalendar.date(from: oldestDate_dateComponents)!
            
            let currentFullDate = Date()
            let currentFullDate_dateComponents = Calendar.current.dateComponents(in: TimeZone.current, from: currentFullDate )

            var currentDate_dateComponents = DateComponents()
            currentDate_dateComponents.year = currentFullDate_dateComponents.year
            currentDate_dateComponents.month = currentFullDate_dateComponents.month
            currentDate_dateComponents.day = currentFullDate_dateComponents.day
            self.currentDate_atCurrentCalendar = localCalendar.date(from: currentDate_dateComponents)!

            
        }
        
        
        func calcBeforeDayItem( existingViewCon:UIViewController! ) {
            
            //previous
            self.beforeDayView = nil
            
            self.beforeDayItem = AssetMan.getBeforeDayItemOf(targetDayItem:self.currentDayItem, appCalendarSetting:parent.appCalendarSetting )

            // print("beforeDayItem.assetloaded ", beforeDayItem.assetLoaded) #1
            if self.beforeDayItem == nil {
                self.beforeDayView = nil
            } else {
                if existingViewCon == nil {
                    self.beforeDayView = UIHostingController(rootView: DayView(calendarID: parent.calendarID, dayItem: self.beforeDayItem ))
                } else {
                    self.beforeDayView = existingViewCon
                }
            }
            
        }

        func calcAfterDayItem( existingViewCon:UIViewController! ) {
            
            //next
            self.afterDayView = nil
            
            self.afterDayItem = AssetMan.getAfterDayItemOf(targetDayItem:self.currentDayItem, appCalendarSetting:parent.appCalendarSetting )

            if self.afterDayItem == nil {
                self.afterDayView = nil
            } else {
                if existingViewCon == nil {
                    self.afterDayView = UIHostingController(rootView: DayView(calendarID: parent.calendarID, dayItem: self.afterDayItem ))
                } else {
                    self.afterDayView = existingViewCon
                }
            }
        
        }
        
    }
}
