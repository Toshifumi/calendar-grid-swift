//
//  Instruction_ImageView.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2021/03/05.
//

import SwiftUI

struct Instruction_ImageView: View {
    var body: some View {
        ZStack {
            VStack {
                Image("Instruction_Swipe")
                Image("Instruction_SetCoverImage")
            }
        }
        .onTapGesture {
            RootViewManager.shared.showInstruction_ImageView = false
            RootViewManager.shared.showInstruction_ImageView_Trigger += 1
        }
    }
}

struct Instruction_ImageView_Previews: PreviewProvider {
    static var previews: some View {
        Instruction_ImageView()
    }
}
