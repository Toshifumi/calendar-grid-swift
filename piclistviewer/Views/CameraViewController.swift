//
//  CameraViewController.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2021/01/27.
//

import SwiftUI
import UIKit

struct CameraViewController: UIViewControllerRepresentable {
    
    @Binding var isShown: Bool
    @Binding var refreshTargetDateString: String
    var calnderID: Int
    
    func makeCoordinator() -> Coordinator {
        return Coordinator(isShown: $isShown, refreshTargetDateString: $refreshTargetDateString, calendar: calnderID)
    }

    
    func makeUIViewController(context: UIViewControllerRepresentableContext<CameraViewController>) -> UIImagePickerController {
        
      let picker = UIImagePickerController()
      picker.delegate = context.coordinator
      /// Default is images gallery. Un-comment the next line of code if you would like to test camera
      picker.sourceType = .camera

        return picker

    }
    
    func updateUIViewController(_ uiViewController: UIImagePickerController,
                                context: UIViewControllerRepresentableContext<CameraViewController>) {
      
    }


    class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
        
        @Binding var isCoordinatorShown: Bool
        @Binding var refreshTargetDateString: String
        var calnderID: Int

        init(isShown: Binding<Bool>, refreshTargetDateString: Binding<String>, calendar: Int) {
            _isCoordinatorShown = isShown
            _refreshTargetDateString = refreshTargetDateString
            calnderID = calendar
        }
        
        func imagePickerController(_ picker: UIImagePickerController,
                                   didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            guard let unwrapImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
            
            AssetManager.shared.saveNewAsset(image: unwrapImage) { (newLocalIdentifier) in
                
                print("calendarID : ", self.calnderID, "  newLocalIdentifier : ", newLocalIdentifier)

                if newLocalIdentifier == "" {
                    self.refreshTargetDateString = ""
                    self.isCoordinatorShown = false
                } else {
                    let today = Date()
                    let localCalendar = Calendar.current
                    let year = localCalendar.component(.year, from: today)
                    let month = localCalendar.component(.month, from: today)
                    let day = localCalendar.component(.day, from: today)

                    DispatchQueue.main.async {
                        RealmManager.shared.setCustomCoverAssetLocalIdentifier(calendarID: self.calnderID, year: year, month: month, day: day, localIdentifier: newLocalIdentifier)
                        self.refreshTargetDateString = String(format: "%04d%02d%02d", year, month, day)
                        print("sent $refreshTargetDateString :" , self.refreshTargetDateString)

                        self.isCoordinatorShown = false
                    }
                }
            }
        }
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            self.refreshTargetDateString = ""
            self.isCoordinatorShown = false
        }
    }

}

