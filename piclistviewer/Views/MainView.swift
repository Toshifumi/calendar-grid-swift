//
//  MainView.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2020/12/15.
//

import SwiftUI
import Photos

struct MainView: View {
    
    @ObservedObject var envData: EnvData

    @State private var showAlert:Bool = false
    @State private var showProgress:Bool = false
    @State private var showInstruction_DayView:Bool = false
    @State private var showInstruction_ImageView:Bool = false
    
    @ObservedObject var RootMan:RootViewManager = RootViewManager.shared
    @ObservedObject var AssetMan = AssetManager.shared
    @ObservedObject var LicMan = LicenseManager.shared
    @State var adProvider = LicenseManager.shared.adProvider

    @State var makeScrollBottom = false

    @State var CalendarView0:CalendarView!
    @State var CalendarView1:CalendarView!
    @State var CalendarView2:CalendarView!
    
    let GBL = Global.shared
    
    @State private var hasPermission = false
    @State private var showDialog = false
    @State private var selectedCalendarID = 0
    
    @State var showCaptureImageView: Bool = false
    @State var refreshTargetDateString: String = ""
    
    @State var showSettingsView: Bool = false
    
    
    init() {
        
        self.envData = EnvData()

    }
    
    
    var body: some View {
        
        ZStack {
            
            VStack(spacing: 0) {
                
                NavigationView {
                    
                    ZStack {
                        
                        if self.hasPermission {
                            
                            ZStack {
                                
                                if !self.envData.CalendarSetting1.useThisCalendar && !self.envData.CalendarSetting2.useThisCalendar {
                                    
                                    CalendarView0
                                    
                                } else {
                                    
                                    TabView(selection: $selectedCalendarID) {
                                        CalendarView0
                                            .tabItem {
                                                VStack {
                                                    Image(uiImage: UIImage(imageLiteralResourceName: (selectedCalendarID == 0 ? "tab0_selected" : "tab0_notSelected")))
                                                        .renderingMode(.original)
                                                    Text(self.envData.CalendarSetting0.calendarName)
                                                        .foregroundColor(.primary)
                                                }
                                            }.tag(0)
                                        
                                        
                                        if self.envData.CalendarSetting1.useThisCalendar {
                                            CalendarView1
                                                .tabItem {
                                                    VStack {
                                                        Image(uiImage: UIImage(imageLiteralResourceName: (selectedCalendarID == 1 ? "tab1_selected" : "tab1_notSelected")))
                                                            .renderingMode(.original)
                                                        Text(self.envData.CalendarSetting1.calendarName)
                                                            .foregroundColor(.primary)
                                                    }
                                                }.tag(1)
                                        }
                                        
                                        if self.envData.CalendarSetting2.useThisCalendar {
                                            CalendarView2
                                                .tabItem {
                                                    VStack {
                                                        Image(uiImage: UIImage(imageLiteralResourceName: (selectedCalendarID == 2 ? "tab2_selected" : "tab2_notSelected")))
                                                            .renderingMode(.original)
                                                        Text(self.envData.CalendarSetting2.calendarName)
                                                            .foregroundColor(.primary)
                                                    }
                                                }.tag(2)
                                        }
                                    }
                                }
                            }
                            
                        } else {
                            
                            ZStack() {
                                
                                CalendarView0
                                
                            }
                            .alert(isPresented: $showDialog) {
                                Alert(title: Text(NSLocalizedString("loc_AlertAction_PhotoAccess_title", comment: "")),
                                      message: Text(NSLocalizedString("loc_AlertAction_PhotoAccess_message", comment: "")),
                                      dismissButton: .default(Text(NSLocalizedString("loc_OK", comment: "")),
                                                              action: {})) // ボタンがタップされた時の処理
                            }
                        }
                    }
                    
                    .navigationBarTitle("CALENDAR GRID")
                    .navigationBarTitleDisplayMode(.inline)
                    .toolbar {
                        /// ナビゲーションバー左
                        ToolbarItem(placement: .navigationBarLeading){
                            
                            VStack {
                                Button(action: {
                                    showCaptureImageView = true
                                }) {
                                    Image(systemName: "camera.fill")
                                        .accentColor(.primary)

                                }
                                .fullScreenCover(isPresented: $showCaptureImageView, onDismiss: {
                                    self.updateLastMonthView()
                                    self.showCaptureImageView = false
                                }, content: {
                                    CameraViewController(isShown: $showCaptureImageView, refreshTargetDateString: $refreshTargetDateString, calnderID:selectedCalendarID )
                                })
                            }
                        }

                        /// ナビゲーションバー右１
                        ToolbarItem(placement: .navigationBarTrailing){
                            HStack {
                                Text("")
                                NavigationLink(destination: SettingView().environmentObject(envData)
                                                .environmentObject(envData)

                                ) {
                                    Image(systemName: "gearshape.fill")
                                }
                            }
                        }
                    }
                }
                .accentColor(.primary)

                if !self.LicMan.isPurchased {
                    if self.adProvider == 2 {
                        AdfurikunView()
                            .frame(width: UIScreen.main.bounds.size.width, height: 50, alignment: .center)
                            .background(Color.gray)
                    } else {
                        AdmobView()
                            .frame(width: UIScreen.main.bounds.size.width, height: 50, alignment: .center)
                            .background(Color.gray)
                    }
                }
                
                
            }
            
            if self.showProgress {
                ProgressView()
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                    .background(Color(red: 1.0, green: 1.0, blue: 1.0, opacity: 0.6))
            }

            if self.showInstruction_DayView {
                Instruction_DaysView()
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                    .background(Color(red: 1.0, green: 1.0, blue: 1.0, opacity: 0.6))
            }

            if self.showInstruction_ImageView {
                Instruction_ImageView()
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                    .background(Color(red: 1.0, green: 1.0, blue: 1.0, opacity: 0.6))
            }

        }
        .onReceive(LicMan.$updateTrigger, perform: { _ in
            if LicMan.updateTrigger != 0 && LicMan.isPurchased {
//                print("LicMan.isPurchased ", LicMan.isPurchased)
                self.envData.refreshCalendarItem()
//                self.updateCalendarViews()
            }
        })
        .onReceive(RootMan.$showInstruction_ImageView_Trigger, perform: { _ in
            self.showInstruction_ImageView = RootMan.showInstruction_ImageView
        })
        .onReceive(RootMan.$showInstruction_DayView_Trigger, perform: { _ in
            self.showInstruction_DayView = RootMan.showInstruction_DayView
        })
        .onReceive(RootMan.$trigger_ProgressView, perform: { _ in
            self.showProgress = RootMan.showProgress
        })
        .onReceive(RootMan.$showAlertTrigger, perform: { _ in
            if RootMan.showAlert {
                self.showAlert = true
                RootMan.showAlert = false
            }
        })
        .onReceive(self.AssetMan.$updateTrigger, perform: { _ in

//            print("AssetMan.$ContentChangedDates.count : ", AssetMan.ContentChangedDates.count )

            if self.AssetMan.ContentChangedDates.count != 0 {

                var doneArray:Array<String> = Array()
                self.AssetMan.ContentChangedDates.forEach { (date) in
                    let dateComponents = Calendar.current.dateComponents(in: TimeZone.current, from: date )
                    let year:Int = dateComponents.year ?? 1970
                    let month:Int = dateComponents.month ?? 1
                    let day:Int = dateComponents.day ?? 1
                    let targetDateString = String(format: "%04d%02d%02d", year, month, day)

                    if doneArray.firstIndex(of: targetDateString) == nil {
                        //完了配列に該当なし -> 更新する
//                        print("AssetMan.ContentChangedDates ", targetDateString)
                        CalendarView0.refreshTargetDayItem(refreshTargetDateString: targetDateString)
                        CalendarView1.refreshTargetDayItem(refreshTargetDateString: targetDateString)
                        CalendarView2.refreshTargetDayItem(refreshTargetDateString: targetDateString)
                        doneArray.append(refreshTargetDateString)
                    } else {
                        //完了配列に同じ日付発見 -> 更新しない
                    }
                }
            }
        })
        .onReceive(self.envData.CalendarSetting0.$refreshCounter, perform: { _ in
            if self.envData.CalendarSetting0.needRefresh {
                CalendarView0 = CalendarView(calendarID: 0, calendarItem: self.envData.CalendarItem0)
                self.envData.CalendarSetting0.needRefresh = false
            }
        })
        .onReceive(self.envData.CalendarSetting1.$refreshCounter, perform: { _ in
            if self.envData.CalendarSetting1.needRefresh {
                CalendarView1 = CalendarView(calendarID: 1, calendarItem: envData.CalendarItem1)
                self.envData.CalendarSetting1.needRefresh = false
            }
        })
        .onReceive(self.envData.CalendarSetting2.$refreshCounter, perform: { _ in
            if self.envData.CalendarSetting2.needRefresh {
                CalendarView2 = CalendarView(calendarID: 2, calendarItem: envData.CalendarItem2)
                self.envData.CalendarSetting2.needRefresh = false
            }
        })

        .onReceive(self.envData.CalendarItem0.$refreshCounter, perform: { _ in
            if self.envData.CalendarItem0.needRefresh {
                CalendarView0 = CalendarView(calendarID: 0, calendarItem: envData.CalendarItem0)
                self.envData.CalendarItem0.needRefresh = false
            }
        })
        .onReceive(self.envData.CalendarItem1.$refreshCounter, perform: { _ in
            if self.envData.CalendarItem1.needRefresh {
                CalendarView1 = CalendarView(calendarID: 1, calendarItem: envData.CalendarItem1)
                self.envData.CalendarItem1.needRefresh = false
            }
        })
        .onReceive(self.envData.CalendarItem2.$refreshCounter, perform: { _ in
            if self.envData.CalendarItem2.needRefresh {
                CalendarView2 = CalendarView(calendarID: 2, calendarItem: envData.CalendarItem2)
                self.envData.CalendarItem2.needRefresh = false
            }
        })
        .alert(isPresented: $showAlert ) {
            RootMan.alertContent
        }
        .onAppear() {
            self.checkPermission()
//            print("onApper has been called ")
//            self.envData.refreshCalendarItem()
//            self.updateCalendarViews()
        }


    }
    
    func updateLastMonthView() {
        
        if self.refreshTargetDateString == "" {
//            print("no need refresh")
        } else {
//            print("target ", self.refreshTargetDateString)
            switch selectedCalendarID {
            case 0:
                CalendarView0.refreshTargetDayItem(refreshTargetDateString: self.refreshTargetDateString)
            case 1:
                CalendarView1.refreshTargetDayItem(refreshTargetDateString: self.refreshTargetDateString)
            case 2:
                CalendarView2.refreshTargetDayItem(refreshTargetDateString: self.refreshTargetDateString)
            default:
                CalendarView0.refreshTargetDayItem(refreshTargetDateString: self.refreshTargetDateString)
            }
        }
    }
    
    
    func checkPermission() {
        
        if PHPhotoLibrary.authorizationStatus() == .authorized {
            //Permissionあり
            self.hasPermission = true
            
            
        } else {
            //            retValue = false
            // フォトライブラリへの認証要求　ダイアログは一度だけ表示され、２回目以降は１っ回目の返答を返す
            PHPhotoLibrary.requestAuthorization { status in
                if status == .authorized {
                    // フォトライブラリを表示する
                    AssetMan.refreshAssetManager()
                    self.envData.refreshCalendarItem()
//                    self.updateCalendarViews()

                    self.hasPermission = true
                    
                } else if status == .denied {
                    // フォトライブラリへのアクセスが許可されていないため、アラートを表示する
                    self.hasPermission = false
                    self.showDialog = true
                    
                }
            }
        }
    }
    
    func updateCalendarViews() {
        CalendarView0 = CalendarView(calendarID: 0, calendarItem: envData.CalendarItem0)
        CalendarView1 = CalendarView(calendarID: 1, calendarItem: envData.CalendarItem1)
        CalendarView2 = CalendarView(calendarID: 2, calendarItem: envData.CalendarItem2)
    }
    
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
            .environmentObject(EnvData())
    }
}
