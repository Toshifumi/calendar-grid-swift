//
//  SettingView.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2021/01/18.
//

import SwiftUI

struct SettingView: View {
    
    let GBL = Global.shared
    let LicMan = LicenseManager.shared
    @EnvironmentObject var envData: EnvData

    @State var isPurchased = LicenseManager.shared.isPurchased
    
    @State var showCalendarNameDialog0 = false
    @State var showCalendarNameDialog1 = false
    @State var showCalendarNameDialog2 = false

    @State var showNumberOfColumnsDialog0 = false
    @State var showNumberOfColumnsDialog1 = false
    @State var showNumberOfColumnsDialog2 = false

    // 有料機能の購入
    //           有料機能の購入(復元)
    // 説明文
    
    // １つ目のカレンダー
    // カレンダーの表示名
    // Calendar1
    // 曜日で揃えず、一行に表示するセル数を指定する  On/Off
    // １行のセル数 : 4
    // 表示する曜日
    // SUN
    // MON
    // TUE
    // WED
    // THE
    // FRI
    // SAT
    // カバー写真のリセット
    //        ボタン
    
    //２つ目のカレンダーを使う  //On/Off
    // カレンダーの表示名
    // Calendar1
    // 曜日で揃えず、一行に表示するセル数を指定する  On/Off
    // １行のセル数 : 4
    // 表示する曜日
    // SUN
    // MON
    // TUE
    // WED
    // THE
    // FRI
    // SAT
    // カバー写真のリセット
    //        ボタン

    //２つ目のカレンダーを使う  //On/Off
    // カレンダーの表示名
    // Calendar1
    // 曜日で揃えず、一行に表示するセル数を指定する  On/Off
    // １行のセル数 : 4
    // 表示する曜日
    // SUN
    // MON
    // TUE
    // WED
    // THE
    // FRI
    // SAT
    // カバー写真のリセット
    //        ボタン
    
    
    
    var body: some View {
        
        List {
            
            if !self.isPurchased {
                Section(header: Text(NSLocalizedString("loc_PremiumFeature", comment: ""))){
                    Text(String(format: NSLocalizedString("loc_PremiumFeature_Description", comment: ""), LicMan.premiumPrice ))
                    HStack {
                        Spacer()
                        Button(action: {
                            self.showPurchaseDialog()
                        }, label: {
                            Text(NSLocalizedString("loc_purchase", comment: ""))
                                .foregroundColor(Color.blue)
                        })
                    }
                    
                }
            }

            Section(header: Text(NSLocalizedString("loc_CalendarForUse_Title", comment: ""))){
                Text(NSLocalizedString("loc_CalendarForUse_Description", comment: ""))
                HStack {
                    Toggle(isOn: self.$envData.CalendarSetting1.useThisCalendar) {
                        Text(NSLocalizedString("loc_CalendarForUse_UseGreenCalendar", comment: ""))
                    }
                    .onReceive(self.envData.CalendarSetting1.$useThisCalendar, perform: { _ in
                        envData.updateTrigger += 1
                    })
                }
                HStack {
                    Toggle(isOn: self.$envData.CalendarSetting2.useThisCalendar) {
                        Text(NSLocalizedString("loc_CalendarForUse_UseBlueCalendar", comment: ""))
                    }
                    .onReceive(self.envData.CalendarSetting2.$useThisCalendar, perform: { _ in
                        envData.updateTrigger += 1
                    })
                }
            }
            
            
            
            
            Section(header: Text(NSLocalizedString("loc_CalendarSetting_Red_Title", comment: ""))){

                HStack {
                    Text(NSLocalizedString("loc_CalendarSetting_CalendarName", comment: ""))
                    Spacer()
                    
                    Button(action: {
                        self.showCalendarNameDialog0 = true
                    }, label: {
                        Text(self.envData.CalendarSetting0.calendarName)
                            .frame(maxWidth: .infinity, alignment: .trailing)
                            .foregroundColor(Color.blue)
                            .alert(isPresented: $showCalendarNameDialog0,
                                   TextFieldAlert(title: NSLocalizedString("loc_CalendarSetting_CalendarName", comment: ""),
                                                  message: "",
                                                  placeholder: self.envData.CalendarSetting0.calendarName,
                                                  keyboardType: .default) { result in
                                    if result != nil {
                                        // Text was accepted
                                        self.envData.CalendarSetting0.calendarName = result ?? "Calendar"
                                        self.envData.updateTrigger += 1
                                    } else {
                                        // The dialog was cancelled
                                    }
                                   })
                    })
                    
                }

                HStack {
                    Toggle(isOn: $envData.CalendarSetting0.showFixedColumns) {
                        Text(NSLocalizedString("loc_CalendarSetting_FixColumns", comment: ""))
                    }
                }
                HStack {
                    Text(NSLocalizedString("loc_CalendarSetting_NumberOfColumns", comment: ""))
                    Spacer()
                    
                    Button(action: {
                        self.showNumberOfColumnsDialog0 = true
                    }, label: {
                        let text_NumberOfColumns = String(format: "%d", self.envData.CalendarSetting0.numberOfColumns)
                        Text(text_NumberOfColumns)
                            .frame(maxWidth: .infinity, alignment: .trailing)
                            .foregroundColor(Color.blue)
                            .alert(isPresented: $showNumberOfColumnsDialog0,
                                   TextFieldAlert(title: NSLocalizedString("loc_CalendarSetting_NumberOfColumns", comment: ""),
                                                  message: "",
                                                  placeholder: text_NumberOfColumns,
                                                  keyboardType: .numberPad) { result in
                                    if result != nil {
                                        // Text was accepted
                                        var newNumber = Int(result ?? "7")
                                        if newNumber! <= 0 {
                                            newNumber = 1
                                        }
                                        if newNumber! >= 8 {
                                            newNumber = 7
                                        }

                                        self.envData.CalendarSetting0.numberOfColumns = newNumber!
                                        self.envData.updateTrigger += 1
                                    } else {
                                        // The dialog was cancelled
                                    }
                                   })
                    })

                    //                    Button(action: {}, label: {
//                        Text(String(format: "%d", appCalendarSetting0.numberOfColumns))
//                    })
                }
                .padding(.leading, 32.0)
                .disabled(!self.envData.CalendarSetting0.showFixedColumns)
                
                Text(NSLocalizedString("loc_CalendarSetting_FixColumns_Description", comment: ""))
                    .font(.footnote)
                    .padding(.leading, 32.0)

                
                Section(header: Text(NSLocalizedString("loc_CalendarSetting_DayOfWeekForDisplay", comment: ""))){
                    HStack {
                        Toggle(isOn: $envData.CalendarSetting0.SUN) {
                            Text(GBL.weekDaySymbols[0])
                        }
                    }
                    .padding(.leading, 32.0)

                    HStack {
                        Toggle(isOn: $envData.CalendarSetting0.MON) {
                            Text(GBL.weekDaySymbols[1])
                        }
                    }
                    .padding(.leading, 32.0)

                    HStack {
                        Toggle(isOn: $envData.CalendarSetting0.TUE) {
                            Text(GBL.weekDaySymbols[2])
                        }
                    }
                    .padding(.leading, 32.0)

                    HStack {
                        Toggle(isOn: $envData.CalendarSetting0.WED) {
                            Text(GBL.weekDaySymbols[3])
                        }
                    }
                    .padding(.leading, 32.0)

                    HStack {
                        Toggle(isOn: $envData.CalendarSetting0.THU) {
                            Text(GBL.weekDaySymbols[4])
                        }
                    }
                    .padding(.leading, 32.0)

                    HStack {
                        Toggle(isOn: $envData.CalendarSetting0.FRI) {
                            Text(GBL.weekDaySymbols[5])
                        }
                    }
                    .padding(.leading, 32.0)

                    HStack {
                        Toggle(isOn: $envData.CalendarSetting0.SAT) {
                            Text(GBL.weekDaySymbols[6])
                        }
                    }
                    .padding(.leading, 32.0)

                }

            }

            Section(header: Text(NSLocalizedString("loc_CalendarSetting_Green_Title", comment: ""))){
                HStack {
                    Text(NSLocalizedString("loc_CalendarSetting_CalendarName", comment: ""))
                    Spacer()
                    Button(action: {
                        self.showCalendarNameDialog1 = true
                    }, label: {
                        Text(self.envData.CalendarSetting1.calendarName)
                            .frame(maxWidth: .infinity, alignment: .trailing)
                            .foregroundColor(Color.blue)
                            .alert(isPresented: $showCalendarNameDialog1,
                                   TextFieldAlert(title: NSLocalizedString("loc_CalendarSetting_CalendarName", comment: ""),
                                                  message: "",
                                                  placeholder: self.envData.CalendarSetting1.calendarName,
                                                  keyboardType: .default) { result in
                                    if result != nil {
                                        // Text was accepted
                                        self.envData.CalendarSetting1.calendarName = result ?? "Calendar"
                                        self.envData.updateTrigger += 1
                                    } else {
                                        // The dialog was cancelled
                                    }
                                   })
                    })
                }

                HStack {
                    Toggle(isOn: self.$envData.CalendarSetting1.showFixedColumns) {
                        Text(NSLocalizedString("loc_CalendarSetting_FixColumns", comment: ""))
                    }
                }

                HStack {
                    Text(NSLocalizedString("loc_CalendarSetting_NumberOfColumns", comment: ""))
                    Spacer()
                    Button(action: {
                        self.showNumberOfColumnsDialog1 = true
                    }, label: {
                        let text_NumberOfColumns = String(format: "%d", self.envData.CalendarSetting1.numberOfColumns)
                        Text(text_NumberOfColumns)
                            .frame(maxWidth: .infinity, alignment: .trailing)
                            .foregroundColor(Color.blue)
                            .alert(isPresented: $showNumberOfColumnsDialog1,
                                   TextFieldAlert(title: NSLocalizedString("loc_CalendarSetting_NumberOfColumns", comment: ""),
                                                  message: "",
                                                  placeholder: text_NumberOfColumns,
                                                  keyboardType: .numberPad) { result in
                                    if result != nil {
                                        // Text was accepted
                                        var newNumber = Int(result ?? "7")
                                        if newNumber! <= 0 {
                                            newNumber = 1
                                        }
                                        if newNumber! >= 8 {
                                            newNumber = 7
                                        }
                                        self.envData.CalendarSetting1.numberOfColumns = newNumber!
                                        self.envData.updateTrigger += 1
                                    } else {
                                        // The dialog was cancelled
                                    }
                                   })
                    })
                }
                .padding(.leading, 32.0)
                .disabled(!self.envData.CalendarSetting1.showFixedColumns)

                Text(NSLocalizedString("loc_CalendarSetting_FixColumns_Description", comment: ""))
                    .font(.footnote)
                    .padding(.leading, 32.0)

                Section(header: Text(NSLocalizedString("loc_CalendarSetting_DayOfWeekForDisplay", comment: ""))){
                    HStack {
                        Toggle(isOn: self.$envData.CalendarSetting1.SUN) {
                            Text(GBL.weekDaySymbols[0])
                        }
                    }
                    .padding(.leading, 32.0)

                    HStack {
                        Toggle(isOn: self.$envData.CalendarSetting1.MON) {
                            Text(GBL.weekDaySymbols[1])
                        }
                    }
                    .padding(.leading, 32.0)

                    HStack {
                        Toggle(isOn: self.$envData.CalendarSetting1.TUE) {
                            Text(GBL.weekDaySymbols[2])
                        }
                    }
                    .padding(.leading, 32.0)

                    HStack {
                        Toggle(isOn: self.$envData.CalendarSetting1.WED) {
                            Text(GBL.weekDaySymbols[3])
                        }
                    }
                    .padding(.leading, 32.0)

                    HStack {
                        Toggle(isOn: self.$envData.CalendarSetting1.THU) {
                            Text(GBL.weekDaySymbols[4])
                        }
                    }
                    .padding(.leading, 32.0)

                    HStack {
                        Toggle(isOn: self.$envData.CalendarSetting1.FRI) {
                            Text(GBL.weekDaySymbols[5])
                        }
                    }
                    .padding(.leading, 32.0)

                    HStack {
                        Toggle(isOn: self.$envData.CalendarSetting1.SAT) {
                            Text(GBL.weekDaySymbols[6])
                        }
                    }
                    .padding(.leading, 32.0)

                }
            }
            .disabled(!self.envData.CalendarSetting1.useThisCalendar)

            
            Section(header: Text(NSLocalizedString("loc_CalendarSetting_Blue_Title", comment: ""))){
                HStack {
                    Text(NSLocalizedString("loc_CalendarSetting_CalendarName", comment: ""))
                    Spacer()
                    Button(action: {
                        self.showCalendarNameDialog2 = true
                    }, label: {
                        Text(self.envData.CalendarSetting2.calendarName)
                            .frame(maxWidth: .infinity, alignment: .trailing)
                            .foregroundColor(Color.blue)
                            .alert(isPresented: $showCalendarNameDialog2,
                                   TextFieldAlert(title: NSLocalizedString("loc_CalendarSetting_CalendarName", comment: ""),
                                                  message: "",
                                                  placeholder: self.envData.CalendarSetting2.calendarName,
                                                  keyboardType: .default) { result in
                                    if result != nil {
                                        // Text was accepted
                                        self.envData.CalendarSetting2.calendarName = result ?? "Calendar"
                                        self.envData.updateTrigger += 1
                                    } else {
                                        // The dialog was cancelled
                                    }
                                   })
                    })
                }

                HStack {
                    Toggle(isOn: self.$envData.CalendarSetting2.showFixedColumns) {
                        Text(NSLocalizedString("loc_CalendarSetting_FixColumns", comment: ""))
                    }
                }

                HStack {
                    Text(NSLocalizedString("loc_CalendarSetting_NumberOfColumns", comment: ""))
                    Spacer()
                    Button(action: {
                        self.showNumberOfColumnsDialog2 = true
                    }, label: {
                        let text_NumberOfColumns = String(format: "%d", self.envData.CalendarSetting2.numberOfColumns)
                        Text(text_NumberOfColumns)
                            .frame(maxWidth: .infinity, alignment: .trailing)
                            .foregroundColor(Color.blue)
                            .alert(isPresented: $showNumberOfColumnsDialog2,
                                   TextFieldAlert(title: NSLocalizedString("loc_CalendarSetting_NumberOfColumns", comment: ""),
                                                  message: "",
                                                  placeholder: text_NumberOfColumns,
                                                  keyboardType: .numberPad) { result in
                                    if result != nil {
                                        // Text was accepted
                                        var newNumber = Int(result ?? "7")
                                        if newNumber! <= 0 {
                                            newNumber = 1
                                        }
                                        if newNumber! >= 8 {
                                            newNumber = 7
                                        }
                                        self.envData.CalendarSetting2.numberOfColumns = newNumber!
                                        self.envData.updateTrigger += 1
                                    } else {
                                        // The dialog was cancelled
                                    }
                                   })
                    })
                }
                .padding(.leading, 32.0)
                .disabled(!self.envData.CalendarSetting2.showFixedColumns)

                Text(NSLocalizedString("loc_CalendarSetting_FixColumns_Description", comment: ""))
                    .font(.footnote)
                    .padding(.leading, 32.0)

                Section(header: Text(NSLocalizedString("loc_CalendarSetting_DayOfWeekForDisplay", comment: ""))){
                    HStack {
                        Toggle(isOn: self.$envData.CalendarSetting2.SUN) {
                            Text(GBL.weekDaySymbols[0])
                        }
                    }
                    .padding(.leading, 32.0)

                    HStack {
                        Toggle(isOn: self.$envData.CalendarSetting2.MON) {
                            Text(GBL.weekDaySymbols[1])
                        }
                    }
                    .padding(.leading, 32.0)

                    HStack {
                        Toggle(isOn: self.$envData.CalendarSetting2.TUE) {
                            Text(GBL.weekDaySymbols[2])
                        }
                    }
                    .padding(.leading, 32.0)

                    HStack {
                        Toggle(isOn: self.$envData.CalendarSetting2.WED) {
                            Text(GBL.weekDaySymbols[3])
                        }
                    }
                    .padding(.leading, 32.0)

                    HStack {
                        Toggle(isOn: self.$envData.CalendarSetting2.THU) {
                            Text(GBL.weekDaySymbols[4])
                        }
                    }
                    .padding(.leading, 32.0)

                    HStack {
                        Toggle(isOn: self.$envData.CalendarSetting2.FRI) {
                            Text(GBL.weekDaySymbols[5])
                        }
                    }
                    .padding(.leading, 32.0)

                    HStack {
                        Toggle(isOn: self.$envData.CalendarSetting2.SAT) {
                            Text(GBL.weekDaySymbols[6])
                        }
                    }
                    .padding(.leading, 32.0)
                }
            }
            .disabled(!self.envData.CalendarSetting2.useThisCalendar)
            
            Section(header: Text("Version")){
                HStack {
                    Spacer()
                    Text(Global.shared.version)
                }
            }
            
            if self.isPurchased {
                Section(header: Text(NSLocalizedString("loc_PremiumFeature", comment: ""))){
                    HStack {
                        Spacer()
                        Text(String(format: NSLocalizedString("loc_alreadyPurchased", comment: ""), LicMan.premiumPrice ))
                    }
                }
            }

        }
        .listStyle(InsetGroupedListStyle())
        .onDisappear() {
            if self.envData.CalendarSetting0.needRefresh {
                self.envData.CalendarSetting0.refreshCounter += 0
            }

            if self.envData.CalendarSetting1.needRefresh {
                self.envData.CalendarSetting1.refreshCounter += 0
            }

            if self.envData.CalendarSetting2.needRefresh {
                self.envData.CalendarSetting2.refreshCounter += 0
            }
        }
            

    }
    
    
    
    func showPurchaseDialog() {
        let alert = Alert(title: Text(NSLocalizedString("loc_AlertAction_PurchaseStart_Title", comment: "")),
                          message: Text(NSLocalizedString("loc_AlertAction_PurchaseStart_Message", comment: "")),
                          primaryButton: .default(Text(NSLocalizedString("loc_goProceed", comment: "")), action: {
                            LicenseManager.shared.purchase()
                          }),
                          secondaryButton: .destructive(Text(NSLocalizedString("loc_Cancel", comment: "")), action: {
                          }))
        
        RootViewManager.shared.alertContent = alert
        RootViewManager.shared.showAlert = true
        RootViewManager.shared.showAlertTrigger += 1

    }
    
}

extension View {
  public func alert(isPresented: Binding<Bool>, _ alert: TextFieldAlert) -> some View {
    AlertWrapper(isPresented: isPresented, alert: alert, content: self)
  }
}

struct SettingView_Previews: PreviewProvider {
    static var previews: some View {
        SettingView().environmentObject(EnvData())
    }
}
