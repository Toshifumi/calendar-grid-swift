//
//  Instruction_DaysView.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2021/03/05.
//

import SwiftUI

struct Instruction_DaysView: View {
    var body: some View {
        
        ZStack {
            Image("Instruction_Swipe")
        }
        .onTapGesture {
            RootViewManager.shared.showInstruction_DayView = false
            RootViewManager.shared.showInstruction_DayView_Trigger += 1
        }
        
    }
}

struct Instruction_DaysView_Previews: PreviewProvider {
    static var previews: some View {
        Instruction_DaysView()
    }
}
