//
//  CalendarView.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2020/12/22.
//

import SwiftUI

struct CalendarView: View {
    
    var calendarID:Int
    var calendarItem:CalendarItem
    var appCalendarSetting:AppCalendarSetting
    var monthItems:Array<MonthItem> = Array()
    var lastMonthItem:MonthItem = MonthItem()
    @ObservedObject var LicMan = LicenseManager.shared

    @State var isScrolled = false
    @State var displayedItem:Array<MonthItem> = Array()
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    
    init(calendarID:Int, calendarItem: CalendarItem) {
        
        self.calendarID = calendarID
        self.calendarItem = calendarItem

        self.appCalendarSetting = calendarItem.appCalendarSetting

        let begin = Date()

        self.monthItems = calendarItem.monthItems.filter { $0.isExistPhotoInThisMonth == true }
        print("CalendarView self.monthItems:", self.monthItems.count)

        if self.monthItems.count != 0 {
            self.lastMonthItem = self.monthItems[self.monthItems.count-1]
        }
        

        let finish = Date()
        print("calendarID:", calendarID,  "  Time : " + (String)(finish.timeIntervalSince1970 - begin.timeIntervalSince1970), "self.monthItems.count * ", self.monthItems.count )
    }

    var body: some View {
        
        VStack(spacing: 0) {
            
            ScrollView(.vertical) {
                ScrollViewReader { value in
                    
                    if (LicMan.isPurchased || LicMan.isTrialMode) {
                    } else {
                        NowRestrictedView()
                    }
                    
                    LazyVStack(spacing: 20) {

                        ForEach( self.monthItems, id: \.self) { item in
                            MonthView(calendarID: self.calendarID, monthItem: item, appCalendarSetting: appCalendarSetting)
                                .id(item)
                                .onAppear() {
                                    self.displayedItem.append(item)
//                                    print("displayed month : ", item.year, "/", item.month)
                                }
                        }
                    }
                    .onReceive(timer) {_ in
                        
                        //Calendar Viewが最初に表示されたときに、画面最下部にある現在月を表示したい
                        //そのため onApperでscrollToをキックするべきなのだが、NavigationView内の
                        //Scrollview、LazeVStackの組み合わせでは、scrollToが実行されない
                        //多分これはswiftUIのBugであるが、回避する必要がある
                        //Viewの組み合わせなどから回避策を探したが、最終的に実現できたのは、CalendarViewが
                        //作成された段階でtimerを実行し、それによりscrollToで最秋月が表示する方法
                        //表示された月は,LazyVStackのonApperで記録し、その配列に最秋月が含まれたかどうかで
                        //判定する。その配列に含まれるまでtimerでscrollToをキックし、表示が確認されたら
                        //timerを終了するロジック。
                        //今後のswiftUIの回収でonApperでscrollToが動作するようになるかもしれないので
                        //それが動作しても問題ないような実装とする
                        
                        print("Recived timer event for scroll to bottom. Calendar:", calendarID)
                        
                        if isScrolled {
                            //スクロール済み。タイマーが動作しているので、キャンセルする
                            self.timer.upstream.connect().cancel()
                        } else {
                            //まだスクロールされていない
//                            print("displayedItem : ", displayedItem)
//                            print("target : ", self.lastMonthItem)

                            let result = self.displayedItem.filter { $0 == self.lastMonthItem }

                            if result.count == 0 {
                                //表示されていない
//                                print("not displayed yet")
                                value.scrollTo(self.lastMonthItem, anchor: .bottom)
                            } else {
//                                print("Find displayed ")
                                self.timer.upstream.connect().cancel()
                                isScrolled = true
                            }
                        }

                    }
                    .onAppear {
                        if isScrolled {

                        } else {
                            let result = self.displayedItem.filter { $0 == self.lastMonthItem }
                            if result.count == 0 {
                                //表示されていない
                                value.scrollTo(self.lastMonthItem, anchor: .bottom)
                            } else {
//                                print("Find displayed ")
                                isScrolled = true
                            }
                        }
                    }
                }
            }

        }
        
    }
    

    func printLog() {
        print("printLog")
    }
    
    
    func refreshTargetDayItem(refreshTargetDateString:String) {
        
        print("target calendar : ", calendarID)
        let target_year = Int(refreshTargetDateString.prefix(4))
        let target_month_string = refreshTargetDateString[refreshTargetDateString.index(refreshTargetDateString.startIndex, offsetBy: 4)...refreshTargetDateString.index(refreshTargetDateString.startIndex, offsetBy: 5)]
        let target_month = Int(target_month_string)
        let target_day = Int(refreshTargetDateString.suffix(2))
        
        monthItems.forEach { (monthItem) in
            if monthItem.year == target_year && monthItem.month == target_month {
                monthItem.items.forEach { (dayItem) in
                    if dayItem.day == target_day {
                        dayItem.assetLoaded = false
                    }
                }
            }
        }
    }
    
}

struct CalendarView_Previews: PreviewProvider {
    static var previews: some View {
        CalendarView(calendarID: 0, calendarItem: CalendarItem(appCalendarSetting: AppCalendarSetting(calendarID: 0), isPlaceHolder: true))
    }
}
