//
//  BannerView_Admob.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2021/02/16.
//

import SwiftUI
import GoogleMobileAds

struct AdmobView: UIViewRepresentable {
    
    func makeUIView(context: Context) -> GADBannerView {
        
        let LicMan = LicenseManager.shared
        let banner = GADBannerView(adSize: kGADAdSizeBanner)
        // 以下は、バナー広告向けのテスト専用広告ユニットIDです。自身の広告ユニットIDと置き換えてください。
        banner.adUnitID = LicMan.AdMob_bannerUnitID
        banner.rootViewController = UIApplication.shared.windows.first?.rootViewController
        banner.load(GADRequest())
        return banner
    }

    func updateUIView(_ uiView: GADBannerView, context: Context) {
    }
}

//struct ContentView: View {
//    var body: some View {
//        AdmobView()
//        // サイズを変更する場合
//        // AdView().frame(width: 320, height: 50)
//    }
//}

struct AdmobView_Previews: PreviewProvider {
    static var previews: some View {
        AdmobView()
    }
}
