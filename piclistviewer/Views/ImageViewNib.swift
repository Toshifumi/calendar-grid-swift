//
//  ImageViewNib.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2021/01/05.
//

import Foundation
import UIKit
import Photos
import SwiftUI
import CropViewController

@objc protocol ImageViewNibDelegate {
    // デリゲートメソッド定義
    func callPop()

}


class ImageViewNib: UIViewController, UIScrollViewDelegate, CropViewControllerDelegate  {
    
    @IBOutlet weak var view_header: UIView!
    @IBOutlet weak var label_headerTitle: UILabel!
    @IBOutlet weak var button_edit: UIButton!
    @IBOutlet weak var button_delete: UIButton!
    
    @IBOutlet weak var view_mainView: UIView!
    @IBOutlet weak var scrollview_mainScroll: UIScrollView!
    @IBOutlet weak var view_contentsView: UIView!

    @IBOutlet weak var playButton: UIButton!
    
    @IBOutlet weak var image_imageView: UIImageView!
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    
    @IBOutlet weak var button_setCover: UIButton!
    
    weak var delegate: ImageViewNibDelegate?

    
    let AssetMan = AssetManager.shared

    var calendarID: Int
    var dayItem: DayItem
    var imageItem:ImageItem

    var imageLocalIdentifier:String = "" //スワイプすると、なぜかimageItemのlocalIdentifierの中身がなくなってしまうので、それを保持するこの変数を別途用意した
    
    // 任意の引数を取る自作のイニシャライザ
    init(calendarID: Int, dayItem:DayItem, imageItem: ImageItem) {
        
        // 受け取った引数でプロパティを初期化
        self.calendarID = calendarID
        self.dayItem = dayItem
        self.imageItem = imageItem
        self.imageLocalIdentifier = imageItem.localIdentifier
        
        // クラスの持つ指定イニシャライザを呼び出す
        super.init(nibName: "ImageViewNib", bundle:Bundle.main )
        
    }
    
    // 新しく init を定義した場合に必須
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.scrollview_mainScroll.delegate = self
        self.scrollview_mainScroll.minimumZoomScale = 1.0
        self.scrollview_mainScroll.maximumZoomScale = 40.0
        
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        DispatchQueue.main.async {
            // https://python5.com/q/shqtzbhz
            // print("self.view_mainView.frame.height : ", self.view_mainView.frame.height)
            self.imageWidth.constant = self.view_mainView.frame.width
            self.imageHeight.constant = self.view_mainView.frame.height

            //以下のコードは、以前はviewdidloadにあったが、上記のsubviewのりサイズ後にロードしないと
            //動画のサイズ、位置がおかしくなる現象があったためにここに移動。
            if self.imageItem.localIdentifier != "" {
                self.loadFullSizeImage(imageItem: self.imageItem)
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        //他のImageViewNibでcustomassetが変更されている可能性があるので、このタイミングで設定を表示する
//        print("this ", imageItem.localIdentifier ,  " -- self.temp ", self.temp,   "  -- custom ", dayItem.customCoverAsset_localIdentifier)
        self.setCoverFlag()
        self.view_header.backgroundColor = UIColor(cmn.CalendarColor_Base(calendarID: calendarID))

        
    }


    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return view_contentsView
    }
    
    func setCoverFlag() {
        
        if cmn.calcSelectedFlag(taregetIdentifier: self.imageLocalIdentifier, dayItem: self.dayItem) {
            self.button_setCover.setImage(UIImage(named: "checke_On"), for: .normal)
        } else {
            self.button_setCover.setImage(UIImage(named: "checke_Off"), for: .normal)
        }
        
    }
    
    func setImageCreationDateToTitle() {
        
        let language = Locale.preferredLanguages.first!
        let locale = Locale(identifier: language)
        
        guard let formatString = DateFormatter.dateFormat(fromTemplate: "yMMMMd eeee", options: 0, locale: locale) else { fatalError() }

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatString
        dateFormatter.locale = locale
        let dateString = dateFormatter.string(from: imageItem.creationDate)
        
        let timeFormatter = DateFormatter()
        timeFormatter.timeStyle = .medium
        let timeString = timeFormatter.string(from: imageItem.creationDate)
        
        self.label_headerTitle.text = dateString + " " + timeString
        
    }
    
    
    @IBAction func tappedPlayButton(_ sender: Any) {
        
        let videoPlayView =  VideoViewNib(localIdentifier: imageItem.localIdentifier)
        videoPlayView.modalPresentationStyle = .fullScreen
        self.present(videoPlayView, animated: true)
        
    }
    

    @IBAction func tapped_SetCoverButton(_ sender: Any) {
        
        if self.dayItem.customCoverAsset_localIdentifier == self.imageLocalIdentifier {
            self.dayItem.customCoverAsset_localIdentifier = "blank"
        } else {
            self.dayItem.customCoverAsset_localIdentifier = self.imageLocalIdentifier
        }
        
        RealmManager.shared.setCustomCoverAssetLocalIdentifier( calendarID:calendarID, year:dayItem.year, month:dayItem.month, day:dayItem.day, localIdentifier: self.dayItem.customCoverAsset_localIdentifier )
        
        let photoFetcher = PhotoFetcher()
        photoFetcher.askDayItemToPhotoLibrary(calendarID: calendarID, dayItem: dayItem)

        self.setCoverFlag()

    }
    
    
    func loadFullSizeImage(imageItem:ImageItem) {
        //ほんらいならAssetManagerに実装すべき内容だが、同時に複数のUIViewControllerから呼び出される可能性があり
        //delegateが上手く動作しない可能性があった。よってUIViewController内に実装し、個々に呼び出す形をとった
        
        DispatchQueue.global().async {
            
            if imageItem.localIdentifier == "" {
                
                imageItem.image = UIImage()
//                DispatchQueue.main.async {
//                    //ロードできず
//                }
                
            } else {
                
                let results = PHAsset.fetchAssets(withLocalIdentifiers: [imageItem.localIdentifier], options: nil)
                if ( results.count != 0  ) {
                    //存在した
                    let phasset = results.firstObject
                    let options = PHImageRequestOptions()
                    options.deliveryMode = .highQualityFormat
                    
                    //写真、もしくはビデオ用のサムネ
                    PHImageManager.default().requestImage(for: phasset!,
                                                          targetSize: PHImageManagerMaximumSize, // CGSize(width: phasset!.pixelWidth, height: phasset!.pixelHeight),
                                                          contentMode: .default,
                                                          options: options,
                                                          resultHandler: { (image, info) in
                                                            
                                                            if image == nil {
                                                                imageItem.image = UIImage()
                                                            } else {
                                                                imageItem.image = image!
                                                                imageItem.creationDate = (phasset?.creationDate)!
                                                            }
                                                            DispatchQueue.main.async {
                                                                if image != nil {
                                                                    self.image_imageView.image = self.imageItem.image
                                                                    self.setImageCreationDateToTitle()

                                                                }
                                                            }
                                                          })
                    
                    //動画の場合、プレイヤーにロード
                    DispatchQueue.main.async {
                        if phasset?.mediaType == PHAssetMediaType.video {
                            imageItem.isVideo = true
                            self.playButton.isHidden = false
                            self.button_edit.isHidden = true
                        } else {
                            self.playButton.isHidden = true
                            self.button_edit.isHidden = false
                        }
                    }
                        
                } else {
                    //存在しない
                    imageItem.image = UIImage()
//                    DispatchQueue.main.async {
//                        //ロードできず
//                    }
                }
            }
        }
    }
    
    @IBAction func tapped_DeleteButton(_ sender: Any) {
        
        AssetMan.deleteAsset(imageItem: imageItem, calendarID: calendarID, dayItem: dayItem, completion: { (success) -> Void in
            if success {
                self.delegate?.callPop()
            }
        })
            
    }
    
    
    @IBAction func tapped_EditButton(_ sender: Any) {
        
        let cropViewController = CropViewController(image: self.imageItem.image)
        cropViewController.delegate = self
        present(cropViewController, animated: true, completion: nil)
        
    }
    
 
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        //加工した画像が取得できる
        
        AssetMan.updateAsset(imageItem: imageItem, calendarID: calendarID, dayItem: dayItem, image: image)

        
        cropViewController.dismiss(animated: true, completion: nil)
        
    }
    
    func cropViewController(_ cropViewController: CropViewController, didFinishCancelled cancelled: Bool) {
        // キャンセル時
        cropViewController.dismiss(animated: true, completion: nil)
    }
    
}
    



