//
//  ImageViewController.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2020/12/29.
//

import SwiftUI
import UIKit

struct ImageViewController: UIViewControllerRepresentable {

    @Environment(\.presentationMode) var presentationMode
    
    var calendarID: Int
    @ObservedObject var dayItem: DayItem

    var items:Array<String>
    var currentIndex:Int

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

    func makeUIViewController(context: Context) -> UIPageViewController {
        let pageViewController = UIPageViewController(
            transitionStyle: .scroll,
            navigationOrientation: .horizontal,
            options: [UIPageViewController.OptionsKey.interPageSpacing : 20])
        pageViewController.dataSource = context.coordinator
        pageViewController.delegate = context.coordinator

        return pageViewController
    }

    func updateUIViewController(_ pageViewController: UIPageViewController, context: Context) {
        pageViewController.setViewControllers(
            [context.coordinator.currentImageView], direction: .forward, animated: true)
    }

    class Coordinator: NSObject, UIPageViewControllerDataSource, UIPageViewControllerDelegate, ImageViewNibDelegate {
        
        let AssetMan = AssetManager.shared
        let localCalendar = Calendar.current
        
        var currentImageItem:ImageItem = ImageItem()
        var beforeImageItem:ImageItem = ImageItem()
        var afterImageItem:ImageItem = ImageItem()

        var currentImageView:UIViewController
        var beforeImageView:UIViewController!
        var afterImageView:UIViewController!

        var parent: ImageViewController

        //delegate
        func callPop() {
            parent.presentationMode.wrappedValue.dismiss()
        }
        

        init(_ imageViewController: ImageViewController) {
            self.parent = imageViewController
            self.currentImageItem.localIdentifier = parent.items[parent.currentIndex]
            let newImageViewNib = ImageViewNib(calendarID: parent.calendarID, dayItem:parent.dayItem, imageItem:currentImageItem)
            self.currentImageView = newImageViewNib
//            self.currentImageView = ImageViewNib(calendarID: parent.calendarID, dayItem:parent.dayItem, imageItem:currentImageItem)
            
            super.init()

            newImageViewNib.delegate = self
            
            self.calcBeforeDayItem(existingViewCon: nil)
            self.calcAfterDayItem(existingViewCon: nil)
        }
        

        func pageViewController(
            _ pageViewController: UIPageViewController,
            viewControllerBefore viewController: UIViewController) -> UIViewController? {
            
            return self.beforeImageView

        }

        func pageViewController(
            _ pageViewController: UIPageViewController,
            viewControllerAfter viewController: UIViewController) -> UIViewController? {

            return self.afterImageView
            
        }

        func pageViewController(
            _ pageViewController: UIPageViewController,
            didFinishAnimating finished: Bool,
            previousViewControllers: [UIViewController],
            transitionCompleted completed: Bool) {
            if completed {

                let visibleViewController = pageViewController.viewControllers?.first

                if visibleViewController == self.beforeImageView {
                    
                    self.currentImageItem = self.beforeImageItem
                    if self.parent.currentIndex == 0 {
                        
                    } else {
                        self.parent.currentIndex -= 1
                    }

                    self.calcBeforeDayItem(existingViewCon: nil)
                    self.calcAfterDayItem(existingViewCon: previousViewControllers.first)

                } else if visibleViewController == self.afterImageView {

                    self.currentImageItem = self.afterImageItem
                    if self.parent.currentIndex == self.parent.items.count-1 {
                        
                    } else {
                        self.parent.currentIndex += 1
                    }

                    self.calcBeforeDayItem(existingViewCon: previousViewControllers.first)
                    self.calcAfterDayItem(existingViewCon: nil)

                } else {
                    
                }
                
            }
        }

      
        
        func calcBeforeDayItem( existingViewCon:UIViewController! ) {
            
            //previous
            self.beforeImageView = nil
            
            if self.parent.currentIndex == 0 {
                self.beforeImageItem.localIdentifier = ""
            } else {
                self.beforeImageItem.localIdentifier = parent.items[parent.currentIndex-1]
            }

            if self.beforeImageItem.localIdentifier == "" {
                self.beforeImageView = nil
            } else {
                if existingViewCon == nil {
                    let newImageViewNib = ImageViewNib(calendarID: parent.calendarID, dayItem:parent.dayItem, imageItem:self.beforeImageItem)
                    newImageViewNib.delegate = self
                    self.beforeImageView = newImageViewNib
                    //self.beforeImageView = UIHostingController(rootView: ImageView(imageItem: self.beforeImageItem))
                } else {
                    self.beforeImageView = existingViewCon
                }
            }
            
        }

        func calcAfterDayItem( existingViewCon:UIViewController! ) {
            
            //next
            self.afterImageView = nil
            
            if self.parent.currentIndex == self.parent.items.count-1 {
                self.afterImageItem.localIdentifier = ""
            } else {
                self.afterImageItem.localIdentifier = parent.items[parent.currentIndex+1]
            }

            if self.afterImageItem.localIdentifier == "" {
                self.afterImageView = nil
            } else {
                if existingViewCon == nil {
                    let newImageViewNib = ImageViewNib(calendarID: parent.calendarID, dayItem:parent.dayItem, imageItem:self.afterImageItem)
                    newImageViewNib.delegate = self

                    self.afterImageView = newImageViewNib
                    //self.afterImageView = UIHostingController(rootView: ImageView(imageItem: self.afterImageItem))
                } else {
                    self.afterImageView = existingViewCon
                }
            }
        
        }
        
    }


}

