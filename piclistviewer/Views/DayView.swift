//
//  DayView.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2020/12/24.
//

import SwiftUI

struct DayView: View {
    

    var calendarID: Int
    @ObservedObject var dayItem: DayItem
    
    
    var body: some View {

        let cellSpace:CGFloat = 1.0
        let screenWidth = UIScreen.main.bounds.size.width
        let cellWidth = (CGFloat)(screenWidth - (cellSpace * 3) ) / 4.0
        
        let items = AssetManager.shared.getLocalIdentifierArrayForTheDay(year: dayItem.year, month: dayItem.month, day: dayItem.day)
        
        
        VStack (spacing: 0) {
            HStack {
                Text( calcTitleDate() )
                    .font(.title2)
                    .padding(.top, 2.0)
                    .padding(.leading, 7.0)
                    .padding(.bottom, 4.0)
                Spacer()
            }
            .background(cmn.CalendarColor_Base(calendarID: calendarID))
            
            Spacer().frame(height: 2.0)

            ScrollView(.vertical) {
                ScrollViewReader { value in
                    LazyVGrid(columns: Array(repeating: GridItem(), count: 4), spacing: cellSpace, pinnedViews: [], content: {
                        
                        ForEach(Array(items.enumerated()), id: \.offset) { index, item in
                            
                            let isCoverImage = cmn.calcSelectedFlag(taregetIdentifier: items[index], dayItem: dayItem)
                            
                            ImageGridCell(calendarID: self.calendarID, dayItem:self.dayItem, items: items, index: index, isCoverImage: isCoverImage)
                                .frame(width: cellWidth, height: cellWidth)
                        }
                        
                    })
                }
            }

            Spacer()
            
        }

    }
    
    
    func calcTitleDate() -> String{
        
        let calendar = Calendar(identifier: .gregorian)
        var targetDay_dateComponents = DateComponents()
        targetDay_dateComponents.year = self.dayItem.year
        targetDay_dateComponents.month = self.dayItem.month
        targetDay_dateComponents.day = self.dayItem.day
        let targetDay_Date:Date = calendar.date(from: targetDay_dateComponents)!
        
        let language = Locale.preferredLanguages.first!
        let locale = Locale(identifier: language)
        
        guard let formatString = DateFormatter.dateFormat(fromTemplate: "yMMMMd eeee", options: 0, locale: locale) else { fatalError() }

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatString
        dateFormatter.locale = locale

        let dateString = dateFormatter.string(from: targetDay_Date)
        
        return dateString
        
    }


}

struct DayView_Previews: PreviewProvider {
    static var previews: some View {
        DayView(calendarID: 0, dayItem: DayItem(year: 2020, month: 12, day: 1) )
    }
}
