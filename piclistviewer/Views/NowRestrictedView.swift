//
//  NowRestrictedView.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2021/02/25.
//

import SwiftUI

struct NowRestrictedView: View {
    
    let LicMan = LicenseManager.shared
    let screenWidth = UIScreen.main.bounds.size.width

    var body: some View {
        ZStack {
            ZStack {
                ZStack {

                    VStack {
                        Text(NSLocalizedString("loc_Restrictmode_title", comment: ""))
                            .font(.title2)
                            .foregroundColor(.blue)

                        Spacer()
                            .frame(height: 20)

                        Text(NSLocalizedString("loc_Restrictmode_description1", comment: ""))
                        Spacer()
                            .frame(height: 20)
                        
                        Text(String(format: NSLocalizedString("loc_Restrictmode_description2", comment: ""), LicMan.premiumPrice ))
                        
                    }
                    .padding(.horizontal, 20.0)
                }

            }
            .frame(width: screenWidth - 20, height: 320 - 20, alignment: .center)
            .background(Color(.systemGray5))

        }
        .frame(width: screenWidth, height: 320, alignment: .center)

    }
    
}

struct NowRestrictedView_Previews: PreviewProvider {
    static var previews: some View {
        NowRestrictedView()
    }
}
