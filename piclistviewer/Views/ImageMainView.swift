//
//  ImageMainView.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2020/12/29.
//

import SwiftUI

struct ImageMainView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var calendarID: Int
    var dayItem: DayItem

    let items:Array<String>
    let currentIndex:Int

    var body: some View {

        ZStack(alignment: .bottomTrailing) {
            ImageViewController( calendarID: self.calendarID, dayItem:self.dayItem,  items: self.items, currentIndex: currentIndex)
        }
        .onAppear() {
            
            if !UserDefaults.standard.bool(forKey: "done_showInstruction_ImageView") {
                RootViewManager.shared.showInstruction_ImageView = true
                RootViewManager.shared.showInstruction_ImageView_Trigger += 1
                UserDefaults.standard.set(true, forKey: "done_showInstruction_ImageView")
            }
            
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: backButton)

    }
    
    var backButton : some View { Button(action: {
        self.presentationMode.wrappedValue.dismiss()
    }) {
        HStack {
            Image(systemName: "chevron.backward")
                .aspectRatio(contentMode: .fit)
            Text("Back") //translated Back button title
        }
    }
    }

}

struct ImageMainView_Previews: PreviewProvider {
    static var previews: some View {
        ImageMainView(calendarID: 0, dayItem:DayItem(), items: [""], currentIndex: 0)
    }
}
