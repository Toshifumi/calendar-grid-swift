//
//  BannerView_adfurikun.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2021/02/16.
//

import SwiftUI
import UIKit
import ADFMovieReward

struct AdfurikunView: UIViewRepresentable {

    let bannerView = UIView()

    func makeUIView(context: Context) -> UIView {
        
        return bannerView
    }

    func updateUIView(_ uiView: UIView, context: Context) {
    }


    func makeCoordinator() -> Coordinator {
        return Coordinator(self)
    }

    class Coordinator: NSObject, ADFmyNativeAdDelegate {

        /// 親View
        let parent: AdfurikunView
        var mADFmyBanner:ADFmyBanner?

        init(_ parent: AdfurikunView) {

            let LicMan = LicenseManager.shared

            self.parent = parent

            super.init()
            
            self.mADFmyBanner = ADFmyBanner.createInstance(LicMan.adfurikun_banner_APPID)
            self.mADFmyBanner?.loadAndNotify(to: self)

//            let mADFmyBanner = ADFmyBanner.createInstance(LicMan.adfurikun_banner_APPID)
//            mADFmyBanner.loadAndNotify(to: self)

            
        }
        

        func onNativeAdLoadFinish(_ info: ADFNativeAdInfo, appID: String) {
            //adfurikun 広告ロード成功
            if let mediaView = info.mediaView {
                mediaView.frame = parent.bannerView.bounds
                parent.bannerView.addSubview(mediaView)
                info.playMediaView()
            }

        }
        
        func onNativeAdLoadError(_ error: ADFMovieError, appID: String) {
            //広告ロード失敗
            print("失敗")
        }
        

    }
    
    
}
//struct BannerView: View {
//    var body: some View {
//        AdfurikunView()
//        // サイズを変更する場合
//        // AdView().frame(width: 320, height: 50)
//    }
//}

struct AdfurikunView_Previews: PreviewProvider {
    static var previews: some View {
        AdfurikunView()
    }
}
