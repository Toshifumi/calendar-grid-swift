//
//  VideoViewNib.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2021/01/12.
//

import Foundation
import UIKit
import Photos
import SwiftUI


class VideoViewNib: UIViewController  {
    
    
    @IBOutlet weak var closeButton: UIButton!
    
    @IBOutlet weak var playButton: UIButton!
    
    @IBOutlet weak var mainView: UIView!
    
    var localIdentifier: String!
    var playerLayer = AVPlayerLayer()

    
    // 任意の引数を取る自作のイニシャライザ
    init(localIdentifier: String) {
        
        // 受け取った引数でプロパティを初期化
        self.localIdentifier = localIdentifier
        
        // クラスの持つ指定イニシャライザを呼び出す
        super.init(nibName: "VideoViewNib", bundle:Bundle.main )
        

    }
    
    // 新しく init を定義した場合に必須
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // 画面を回転させるかどうか
    override var shouldAutorotate: Bool {
        return true
    }

    // 回転方向の指定
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .all
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadVideo()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.mainView.layoutSubviews()

        self.playerLayer.videoGravity = .resizeAspect
        self.playerLayer.frame = self.mainView.bounds
        self.playerLayer.player?.play()

        NotificationCenter.default.addObserver(self, selector:#selector(self.playerDidFinishPlaying(note:)),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerLayer.player?.currentItem)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        print(playerLayer.frame)

    }
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//
//        playerLayer.frame = bounds
//
//        if PlayButton != nil {
//            PlayButton.removeFromSuperview()
//        }
//        PlayButton = UIButton(frame: CGRect(x: 16, y: bounds.height - 16 - 60, width: 60, height: 60))
//        PlayButton.setImage(UIImage(named: "pauseVideoButton"), for: .normal)
//        PlayButton.addTarget(self, action: #selector(buttonEvent), for: .touchUpInside)
//        addSubview(PlayButton)
//
//    }



    func loadVideo() {
        
        let results = PHAsset.fetchAssets(withLocalIdentifiers: [self.localIdentifier], options: nil)
        if ( results.count != 0  ) {
            //存在した
            let phasset = results.firstObject
            let options = PHImageRequestOptions()
            options.deliveryMode = .highQualityFormat
            
            //動画の場合、プレイヤーにロード
            if phasset?.mediaType == PHAssetMediaType.video {
                //動画
                PHImageManager.default().requestPlayerItem(forVideo: phasset!, options: nil) { (video, info) in
                    if video != nil {
                        
                        let player = AVPlayer.init(playerItem: video)
                        let playerLayer = AVPlayerLayer(player: player)
                        playerLayer.videoGravity = .resizeAspect
//                        self.playerLayer.frame = self.mainView.bounds
                        self.mainView.layer.addSublayer(playerLayer)
                        self.playerLayer = playerLayer

                        NotificationCenter.default.addObserver(self, selector:#selector(self.playerDidFinishPlaying(note:)),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerLayer.player?.currentItem)
//                        player.play()
                        
//                        print(playerLayer.frame)

                    }
                }
            }
                
        } else {
            //存在しない
        }
    }
    
    
    @IBAction func tappedCloseButton(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    @IBAction func tappedPlayButton(_ sender: Any) {
        
        if playerLayer.player?.timeControlStatus == AVPlayer.TimeControlStatus.playing  {
            //演奏中 -> 停止する
            playButton.setImage(UIImage(named: "playVideoButton"), for: .normal)
            playerLayer.player?.pause()
//                    playerLayer.player?.removeObserver(self, forKeyPath: "rate")
            NotificationCenter.default.removeObserver(self)
            
        } else {
            //停止中 ー＞再生する
            playButton.setImage(UIImage(named: "pauseVideoButton"), for: .normal)

            //                if PlayerLayer.player?.rate == 1 {
            //                    PlayerLayer.player?.seek(to: .zero)
            //                }
            playerLayer.player?.play()
//                    playerLayer.player?.addObserver(self, forKeyPath: "rate", options: [], context: nil)
            NotificationCenter.default.addObserver(self, selector:#selector(self.playerDidFinishPlaying(note:)),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerLayer.player?.currentItem)
            
        }
    }
    
    @objc func playerDidFinishPlaying(note: NSNotification){
        //Called when player finished playing
        playerLayer.player?.seek(to: .zero)
        playButton.setImage(UIImage(named: "playVideoButton"), for: .normal)
//            playerLayer.player?.removeObserver(self, forKeyPath: "rate")
        NotificationCenter.default.removeObserver(self)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        //.AVPlayerからのmessageハンドリングのため必要
    }

    
}

