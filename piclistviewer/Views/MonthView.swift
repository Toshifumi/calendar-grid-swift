//
//  MonthView.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2020/12/24.
//

import SwiftUI

struct MonthView: View {

    let GBL = Global.shared
    var calendarID: Int
    @ObservedObject var monthItem: MonthItem
    @ObservedObject var appCalendarSetting:AppCalendarSetting
    
    
    var body: some View {
        
        let cellSpace:CGFloat = 1.0
        let screenWidth = UIScreen.main.bounds.size.width
        let cellColumns = self.clacCellColumns()
        let cellWidth = (CGFloat)(screenWidth - (cellSpace * (CGFloat)(cellColumns - 1)) ) / (CGFloat)(cellColumns)
        
        let cellHeight:CGFloat = self.calcCellHeight(cellWidth: cellWidth)
        
        let baseColor = cmn.CalendarColor_Base(calendarID: calendarID)
        
        
        VStack {
            HStack {
                //                Text( String(format: "%04d/%02d", self.monthItem.year, self.monthItem.month))
                Text(calcTitleDate())
                    .font(.title2)
                    .padding(.top, 2.0)
                    .padding(.leading, 7.0)
                    .padding(.bottom, 2.0)
                Spacer()
            }
            .background(baseColor)
            
            
            if !self.appCalendarSetting.showFixedColumns {
                
                Spacer().frame(height: 2.0)
                
                LazyVGrid(columns: Array(repeating: GridItem(), count: cellColumns), spacing: cellSpace, pinnedViews: [], content: {
                    
                    if self.appCalendarSetting.SUN {
                        Text( GBL.shortWeekDaySymbols[0])
                            .font(.system(size: 12, weight: .regular))
                            .frame(width: cellWidth, height: 16.0)
                            .background(baseColor)
                    }
                    
                    if self.appCalendarSetting.MON {
                        Text(GBL.shortWeekDaySymbols[1])
                            .font(.system(size: 12, weight: .regular))
                            .frame(width: cellWidth, height: 16.0)
                            .background(baseColor)
                    }
                    
                    if self.appCalendarSetting.TUE {
                        Text(GBL.shortWeekDaySymbols[2])
                            .font(.system(size: 12, weight: .regular))
                            .frame(width: cellWidth, height: 16.0)
                            .background(baseColor)
                    }
                    
                    if self.appCalendarSetting.WED {
                        Text(GBL.shortWeekDaySymbols[3])
                            .font(.system(size: 12, weight: .regular))
                            .frame(width: cellWidth, height: 16.0)
                            .background(baseColor)
                    }
                    
                    if self.appCalendarSetting.THU {
                        Text(GBL.shortWeekDaySymbols[4])
                            .font(.system(size: 12, weight: .regular))
                            .frame(width: cellWidth, height: 16.0)
                            .background(baseColor)
                    }
                    
                    if self.appCalendarSetting.FRI {
                        Text(GBL.shortWeekDaySymbols[5])
                            .font(.system(size: 12, weight: .regular))
                            .frame(width: cellWidth, height: 16.0)
                            .background(baseColor)
                    }
                    
                    if self.appCalendarSetting.SAT {
                        Text(GBL.shortWeekDaySymbols[6])
                            .font(.system(size: 12, weight: .regular))
                            .frame(width: cellWidth, height: 16.0)
                            .background(baseColor)
                    }
                    
                })
                
            }
            
            Spacer().frame(height: 2.0)
            
            LazyVGrid(columns: Array(repeating: GridItem(), count: cellColumns), spacing: cellSpace, pinnedViews: [], content: {
                
                
                ForEach(( self.monthItem.items ), id: \.self) { dayItem in
                    if self.appCalendarSetting.showFixedColumns {
                        if dayItem.day != 0 {
                            DayGridCell(calendarID: self.calendarID, dayItem: dayItem, appCalendarSetting:self.appCalendarSetting)
                                .frame(width: cellWidth, height: cellHeight)
                        }
                    } else {
                        DayGridCell(calendarID: self.calendarID, dayItem: dayItem, appCalendarSetting:self.appCalendarSetting)
                            .frame(width: cellWidth, height: cellHeight)
                    }
                    
                }
                
            })
            //            .background(Color.init(UIColor.lightGray))
            
        }
        .background(Color(.systemBackground))
        
    }
    
    func clacCellColumns() -> Int {
        
        var retValue = 0
        
        if self.appCalendarSetting.showFixedColumns {
            retValue = self.appCalendarSetting.numberOfColumns
        } else {
            retValue = self.appCalendarSetting.enabledDays
        }
        
        return retValue
    }
    
    func calcCellHeight(cellWidth: CGFloat) -> CGFloat {
        
        var retValue:CGFloat = 0
        
        if self.appCalendarSetting.showFixedColumns {
            retValue = cellWidth
        } else {
            if self.appCalendarSetting.enabledDays == 7 {
                retValue = cellWidth * 1.25
            } else {
                retValue = cellWidth
            }
        }
        
        return retValue
        
    }
    
    func calcTitleDate() -> String{
        
        let calendar = Calendar(identifier: .gregorian)
        var targetDay_dateComponents = DateComponents()
        targetDay_dateComponents.year = self.monthItem.year
        targetDay_dateComponents.month = self.monthItem.month
        targetDay_dateComponents.day = 3
        let targetDay_Date:Date = calendar.date(from: targetDay_dateComponents)!
        
        let language = Locale.preferredLanguages.first!
        let locale = Locale(identifier: language)
        
        
        guard let formatString = DateFormatter.dateFormat(fromTemplate: "yMMMM", options: 0, locale: locale) else { fatalError() }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatString
        dateFormatter.locale = locale
        
        let dateString = dateFormatter.string(from: targetDay_Date)
        
        
        return dateString
        
    }
    
//    func getWeekdaySymbols() -> Array<String> {
//
//        let dateFormatter = DateFormatter()
//        let language = Locale.preferredLanguages.first!
//        let locale = Locale(identifier: language)
//        dateFormatter.locale = locale
//
//        return dateFormatter.shortWeekdaySymbols
//
//    }
    
    
}

struct MonthView_Previews: PreviewProvider {
    static var previews: some View {
        MonthView(calendarID: 0, monthItem: MonthItem( year: 2020, month: 11, appCalendarSetting: AppCalendarSetting(calendarID: 0)), appCalendarSetting: AppCalendarSetting(calendarID: 0) )
    }
}
