//
//  LocalClass.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2020/12/15.
//

import SwiftUI
import Foundation
import Photos
//import Combine

//class Coordinator: NSObject, ObservableObject, PHPhotoLibraryChangeObserver {
//
//    @Published var isNeedRefresh = false
//
//    override init() {
//        super.init()
//        PHPhotoLibrary.shared().register(self)
//    }
//
//    func photoLibraryDidChange(_ changeInstance: PHChange) {
//        updateStatus()
//    }
//
//    private func updateStatus() {
//        DispatchQueue.main.async {
//            self.isNeedRefresh = true
//        }
//    }
//}

class ScrollTrigger: ObservableObject {
    enum Action {
        case end
        case top
    }
    @Published var direction: Action? = nil
}



@objc(WBTabCoverAsset)
class WBTabCoverAsset : NSObject, NSCoding {
    
//    static var supportsSecureCoding: Bool = true
    
    var yyyyMMdd:NSString = ""
    var year:NSInteger = 1790
    var month:NSInteger = 1
    var day:NSInteger = 0
    var localIdentifier:NSString = ""

    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.yyyyMMdd, forKey: "yyyyMMdd")
        aCoder.encode(self.year, forKey: "year")
        aCoder.encode(self.month, forKey: "month")
        aCoder.encode(self.day, forKey: "day")
        aCoder.encode(self.localIdentifier, forKey: "localIdentifier")
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        self.yyyyMMdd = aDecoder.decodeObject(forKey: "yyyyMMdd") as! NSString
        self.year = aDecoder.decodeInteger(forKey: "year")
        self.month = aDecoder.decodeInteger(forKey: "month")
        self.day = aDecoder.decodeInteger(forKey: "day")
        self.localIdentifier = aDecoder.decodeObject(forKey: "localIdentifier") as! NSString

    }
    
}


class EnvData: ObservableObject {
    
    @Published var updateTrigger:Int = 0
    
    var CalendarSetting0 = AppCalendarSetting(calendarID: 0)
    var CalendarSetting1 = AppCalendarSetting(calendarID: 1)
    var CalendarSetting2 = AppCalendarSetting(calendarID: 2)

    var CalendarItem0:CalendarItem
    var CalendarItem1:CalendarItem
    var CalendarItem2:CalendarItem

    init() {
        
        CalendarItem0 = CalendarItem(appCalendarSetting: CalendarSetting0, isPlaceHolder: false)
        CalendarItem1 = CalendarItem(appCalendarSetting: CalendarSetting1, isPlaceHolder: false)
        CalendarItem2 = CalendarItem(appCalendarSetting: CalendarSetting2, isPlaceHolder: false)
    }
    
    
    func refreshCalendarItem()    {
        
        CalendarItem0.refreshCalendarItem()
        CalendarItem1.refreshCalendarItem()
        CalendarItem2.refreshCalendarItem()
        
    }
    
    
    
}

class AppCalendarSetting:ObservableObject {
    
    var calendarID:Int
    var calendarPrefix:String
    var enabledDays:Int = 1
    
    var needRefresh = false
    @Published var refreshCounter = 0

    @Published var useThisCalendar:Bool {
        didSet {
            UserDefaults.standard.set(useThisCalendar, forKey: self.calendarPrefix + "useThisCalendar")
        }
    }

    @Published var calendarName:String {
        didSet {
            UserDefaults.standard.set(calendarName, forKey: self.calendarPrefix + "calendarName")
        }
    }

    @Published var showFixedColumns:Bool {
        didSet {
            UserDefaults.standard.set(showFixedColumns, forKey: self.calendarPrefix + "showFixedColumns")
            self.needRefresh = true
        }
    }

    @Published var numberOfColumns:Int {
        didSet {
            UserDefaults.standard.set(numberOfColumns, forKey: self.calendarPrefix + "numberOfColumns")
            self.needRefresh = true
        }
    }

    @Published var SUN:Bool {
        didSet {
            UserDefaults.standard.set(SUN, forKey: self.calendarPrefix + "SUN")
            self.calcEnabledDays()
            self.needRefresh = true
        }
    }

    @Published var MON:Bool {
        didSet {
            UserDefaults.standard.set(MON, forKey: self.calendarPrefix + "MON")
            self.calcEnabledDays()
            self.needRefresh = true
        }
    }

    @Published var TUE:Bool {
        didSet {
            UserDefaults.standard.set(TUE, forKey: self.calendarPrefix + "TUE")
            self.calcEnabledDays()
            self.needRefresh = true
       }
    }

    @Published var WED:Bool {
        didSet {
            UserDefaults.standard.set(WED, forKey: self.calendarPrefix + "WED")
            self.calcEnabledDays()
            self.needRefresh = true
        }
    }

    @Published var THU:Bool {
        didSet {
            UserDefaults.standard.set(THU, forKey: self.calendarPrefix + "THU")
            self.calcEnabledDays()
            self.needRefresh = true
        }
    }

    @Published var FRI:Bool {
        didSet {
            UserDefaults.standard.set(FRI, forKey: self.calendarPrefix + "FRI")
            self.calcEnabledDays()
            self.needRefresh = true
        }
    }

    @Published var SAT:Bool {
        didSet {
            UserDefaults.standard.set(SAT, forKey: self.calendarPrefix + "SAT")
            self.calcEnabledDays()
            self.needRefresh = true
        }
    }

    /// 初期化処理
    init(calendarID:Int) {
        
        self.calendarID = calendarID
        self.calendarPrefix = String(format: "calendar%d_", self.calendarID)
        self.useThisCalendar = UserDefaults.standard.object(forKey: self.calendarPrefix + "useThisCalendar") as? Bool ?? true
        self.calendarName = UserDefaults.standard.string(forKey: self.calendarPrefix + "calendarName") ?? "Calendar"
        self.showFixedColumns = UserDefaults.standard.object(forKey: self.calendarPrefix + "showFixedColumns") as? Bool ?? false
        self.numberOfColumns = UserDefaults.standard.object(forKey: self.calendarPrefix + "numberOfColumns") as? Int ?? 5
        self.needRefresh = false
        self.refreshCounter = 0

        self.SUN = UserDefaults.standard.object(forKey: self.calendarPrefix + "SUN") as? Bool ?? true
        self.MON = UserDefaults.standard.object(forKey: self.calendarPrefix + "MON") as? Bool ?? true
        self.TUE = UserDefaults.standard.object(forKey: self.calendarPrefix + "TUE") as? Bool ?? true
        self.WED = UserDefaults.standard.object(forKey: self.calendarPrefix + "WED") as? Bool ?? true
        self.THU = UserDefaults.standard.object(forKey: self.calendarPrefix + "THU") as? Bool ?? true
        self.FRI = UserDefaults.standard.object(forKey: self.calendarPrefix + "FRI") as? Bool ?? true
        self.SAT = UserDefaults.standard.object(forKey: self.calendarPrefix + "SAT") as? Bool ?? true
        
        self.checkOldSetting()
        
        self.calcEnabledDays()

    }
    
    func calcEnabledDays() {
        self.enabledDays = 0
        if self.SUN { self.enabledDays+=1 }
        if self.MON { self.enabledDays+=1 }
        if self.TUE { self.enabledDays+=1 }
        if self.WED { self.enabledDays+=1 }
        if self.THU { self.enabledDays+=1 }
        if self.FRI { self.enabledDays+=1 }
        if self.SAT { self.enabledDays+=1 }
    }
    
    func checkOldSetting() {
        
        let key = String(format: "isCheckOldSetting%d", calendarID)
        
        if UserDefaults.standard.bool(forKey: key) {return}
        
        self.calendarName = UserDefaults.standard.string(forKey: String(format: "Tab%d.Title", calendarID)) ?? "Calendar"

        self.SUN = UserDefaults.standard.object(forKey: String(format: "Tab%ld.Sunday", calendarID)) as? Bool ?? true
        self.MON = UserDefaults.standard.object(forKey: String(format: "Tab%ld.Monday", calendarID)) as? Bool ?? true
        self.TUE = UserDefaults.standard.object(forKey: String(format: "Tab%ld.Tuesday", calendarID)) as? Bool ?? true
        self.WED = UserDefaults.standard.object(forKey: String(format: "Tab%ld.Wednesday", calendarID)) as? Bool ?? true
        self.THU = UserDefaults.standard.object(forKey: String(format: "Tab%ld.Thursday", calendarID)) as? Bool ?? true
        self.FRI = UserDefaults.standard.object(forKey: String(format: "Tab%ld.Friday", calendarID)) as? Bool ?? true
        self.SAT = UserDefaults.standard.object(forKey: String(format: "Tab%ld.Saturday", calendarID)) as? Bool ?? true

        UserDefaults.standard.set(self.calendarName, forKey: self.calendarPrefix + "calendarName")
        UserDefaults.standard.set(self.SUN, forKey: self.calendarPrefix + "SUN")
        UserDefaults.standard.set(self.MON, forKey: self.calendarPrefix + "MON")
        UserDefaults.standard.set(self.TUE, forKey: self.calendarPrefix + "TUE")
        UserDefaults.standard.set(self.WED, forKey: self.calendarPrefix + "WED")
        UserDefaults.standard.set(self.THU, forKey: self.calendarPrefix + "THU")
        UserDefaults.standard.set(self.FRI, forKey: self.calendarPrefix + "FRI")
        UserDefaults.standard.set(self.SAT, forKey: self.calendarPrefix + "SAT")

        UserDefaults.standard.set(true, forKey: key )
        
    }
    
    
}


class ImageItem {

    var creationDate:Date = Date(timeIntervalSince1970: 0)
    var localIdentifier: String = ""
    var isVideo:Bool = false
    var image: UIImage = UIImage()

}

class ImagesInfoForDay {
    
    var localIdentifier: String = ""
    var numberOfImages: Int = 0
    
}




class CalendarItem: NSObject, ObservableObject {
    
    let AssetMan = AssetManager.shared
    public var monthItems: Array<MonthItem>
    
    var yearOfOldestAsset: Int
    var monthOfOldestAsset: Int
    var appCalendarSetting:AppCalendarSetting
    var isPlaceHolder:Bool
    var needRefresh:Bool
    @Published var refreshCounter: Int

    
    init(appCalendarSetting:AppCalendarSetting, isPlaceHolder:Bool) {
        
        self.monthItems = Array()
        self.yearOfOldestAsset = 1970
        self.monthOfOldestAsset = 1
        self.appCalendarSetting = appCalendarSetting
        self.isPlaceHolder = isPlaceHolder
        self.needRefresh = false
        self.refreshCounter = 0
        
        super.init()
        
        self.refreshCalendarItem()
        
    }
    
    func refreshCalendarItem() {
        
        self.getOldestMonth()
        self.generateAllMonthesItems()
        
        if !self.isPlaceHolder {
            
            self.checkIsExistPhoto() { (result) in
                if result {
                    self.needRefresh = true
                    self.refreshCounter += 1
                    print("result: ",  self.appCalendarSetting.calendarID, result)
                }
            }
            
        }

    }
    
    func getOldestMonth() {
    
        let oldestAssetDate = AssetMan.oldestAssetDate
        let dateComponents = Calendar.current.dateComponents(in: TimeZone.current, from: oldestAssetDate )
        self.yearOfOldestAsset = dateComponents.year ?? 1970
        self.monthOfOldestAsset = dateComponents.month ?? 1

    }

    
    func generateAllMonthesItems() {
        
        self.monthItems = Array()

        var localCalendar = Calendar.current
        localCalendar.firstWeekday = 1 // 日曜日を開始曜日
        localCalendar.minimumDaysInFirstWeek = 4 //初週の設定をISO方式(1/4を含む週)に設定
        
        // 年月日をとりだす
        let today = Date()
        let dateComponents = localCalendar.dateComponents(in: TimeZone.current, from: today )
        let year = dateComponents.year ?? 1970
        let month = dateComponents.month ?? 1
        
        var tempYear = year
        var tempMonth = month
        let LicMan = LicenseManager.shared
        
        while ( String(format: "%04d%02d", tempYear, tempMonth) >= String(format: "%04d%02d", self.yearOfOldestAsset, self.monthOfOldestAsset)) {

            let newItem = MonthItem(year: tempYear, month: tempMonth, appCalendarSetting: appCalendarSetting)

//            if (newItem.year == year && newItem.month == month) {
//                newItem.isExistPhotoInThisMonth = true //今日の月はデータがなくても画面に表示させるため、強制的にtrueを入れる
//            }

            if (LicMan.isPurchased || LicMan.isTrialMode) {
                self.monthItems.insert(newItem, at: 0)
            } else {
                if self.monthItems.count < 2 {
                    self.monthItems.insert(newItem, at: 0)
                }
            }

            tempMonth -= 1
            if tempMonth < 1 {
                tempYear -= 1
                tempMonth += 12
            }

        }
        
        print("Number of MonthItem " , self.monthItems.count)
        
    }
    
    func checkIsExistPhoto(completion: @escaping (Bool) -> ()) {
        
        let group = DispatchGroup()
        //        let queue = DispatchQueue(label: "com.piclistviewer.queue")
        let queue = DispatchQueue(label: "com.piclistviewer.queue", attributes: .concurrent)

        var localCalendar = Calendar.current
        localCalendar.firstWeekday = 1 // 日曜日を開始曜日
        localCalendar.minimumDaysInFirstWeek = 4 //初週の設定をISO方式(1/4を含む週)に設定
        
        // 年月日をとりだす
        let today = Date()
        let dateComponents = localCalendar.dateComponents(in: TimeZone.current, from: today )
        let year = dateComponents.year ?? 1970
        let month = dateComponents.month ?? 1


        let beginDate = Date()
        
        for item in self.monthItems {

            group.enter()
            queue.async(group: group) {

                if (item.year == year && item.month == month) {
                    item.isExistPhotoInThisMonth = true //今日の月はデータがなくても画面に表示させるため、強制的にtrueを入れる
                } else {
                    item.checkIsExistPhotoInThisMonth()
                }

//                print("checking : ", self.appCalendarSetting.calendarID, item.year, item.month)
                group.leave()
            }

        }
        
        
        group.notify(queue: .main) {

            self.refreshCounter += 1

            let compDate = Date()
//                    print("sssss 時間 : " + (String)(compDate.timeIntervalSince1970 - beginDate.timeIntervalSince1970) )
            print("done checking : ", self.appCalendarSetting.calendarID,  (String)(compDate.timeIntervalSince1970 - beginDate.timeIntervalSince1970))

            completion(true)

//            let subMonthItems = self.monthItems.filter { $0.isExistPhotoInThisMonth == true }
//            print("number of items ", subMonthItems.count )
            
            
        }
        
        
        
    }

    
}



class MonthItem: NSObject, ObservableObject {
    
    var year: Int
    var month: Int

    var isExistPhotoInThisMonth: Bool
    var items: Array<DayItem>
    
    var appCalendarSetting:AppCalendarSetting

    @Published var refreshCounter: Int

    
    override init() {
        
        self.year = 1970
        self.month = 1
        
        self.isExistPhotoInThisMonth = true
        self.appCalendarSetting = AppCalendarSetting(calendarID: 0)
        self.items = Array()
        
        self.refreshCounter = 0
        
        super.init()
        
    }
    
    
    init(year:Int, month:Int, appCalendarSetting:AppCalendarSetting) {
        
        self.year = year
        self.month = month

        self.isExistPhotoInThisMonth = true
        self.appCalendarSetting = appCalendarSetting
        self.refreshCounter = 0

        var localCalendar = Calendar.current
        localCalendar.firstWeekday = 1 // 日曜日を開始曜日
        localCalendar.minimumDaysInFirstWeek = 4 //初週の設定をISO方式(1/4を含む週)に設定
        
        //月初日の曜日
        //指定した日付が何曜日か取得（1=日曜日〜7=土曜日）
        // 年月日をとりだす
        var dateComponents = DateComponents()
        dateComponents.year = year
        dateComponents.month = month
        dateComponents.day = 1
        let firstDayDate:Date = localCalendar.date(from: dateComponents)!
        
        // 日数
        let daysInMonth = localCalendar.range(of: .day, in: .month, for: firstDayDate)
        self.items = Array()
        

        //設定反映前、すべての日を一旦登録する
        for i in 1..<daysInMonth!.count+1 {
            let dayItem = DayItem(year: year, month: month, day: i)
            self.items.append(dayItem)
        }

        //設定に応じた、非表示曜日を削除する
        let firstDayItemWeekOfDay = self.items.first!.weekOfDay
            
        //月初の空白セルを追加
        if 1 < firstDayItemWeekOfDay {
            for i in 1..<firstDayItemWeekOfDay {
                let preItem = DayItem()
                preItem.weekOfDay = firstDayItemWeekOfDay - i
                self.items.insert(preItem, at: 0)
            }
        }
            
        //月末の空白セルを追加
        let finalWeekOfDay = self.items[self.items.count-1].weekOfDay
        if finalWeekOfDay != 7 {
            for i in (finalWeekOfDay+1)...7 {
                let postItem = DayItem()
                postItem.weekOfDay = i
                self.items.append(postItem)
            }
        }
            
        //設定で非表示の曜日を削除
        if !appCalendarSetting.SUN {
            self.items = self.items.filter { $0.weekOfDay != 1 }
        }
        if !appCalendarSetting.MON {
            self.items = self.items.filter { $0.weekOfDay != 2 }
        }
        if !appCalendarSetting.TUE {
            self.items = self.items.filter { $0.weekOfDay != 3 }
        }
        if !appCalendarSetting.WED {
            self.items = self.items.filter { $0.weekOfDay != 4 }
        }
        if !appCalendarSetting.THU {
            self.items = self.items.filter { $0.weekOfDay != 5 }
        }
        if !appCalendarSetting.FRI {
            self.items = self.items.filter { $0.weekOfDay != 6 }
        }
        if !appCalendarSetting.SAT {
            self.items = self.items.filter { $0.weekOfDay != 7 }
        }

        //月初に空白セルが一行となっているかを確認、あれば削除
        var count = 0
        for dayItem in self.items {
            if dayItem.day == 0 {
                count += 1
            } else {
                break
            }
        }
        if count >= appCalendarSetting.enabledDays {
            //１行以上の空白セルがある
            for _ in 0..<appCalendarSetting.enabledDays {
                self.items.removeFirst()
            }
        }
        
        //月末に空白セルが一行となっているかを確認、あれば削除
        count = 0
        for dayItem in self.items.reversed() {
            if dayItem.day == 0 {
                count += 1
            } else {
                break
            }
        }
        if count >= appCalendarSetting.enabledDays {
            //１行以上の空白セルがある
            for _ in 0..<appCalendarSetting.enabledDays {
                self.items.removeLast()
            }
        }
        
        super.init()

    }
    
    func checkIsExistPhotoInThisMonth() {
        
        //        let beginDate = Date()
        self.isExistPhotoInThisMonth = AssetManager.shared.isExistPhotoAtThisMonth2(year: year, month: month, appCalendarSetting:appCalendarSetting) //0.93sec
        //        let compDate = Date()
        //        print("実行時間 : " + (String)(compDate.timeIntervalSince1970 - beginDate.timeIntervalSince1970) )

    }
    
}



class DayItem: NSObject, ObservableObject {
    
    var year: Int
    var month: Int
    var day: Int
    var weekOfDay: Int
    var numberOfImages: Int
    var defaultCoverAsset_localIdentifier: String
    @Published var customCoverAsset_localIdentifier: String
    @Published var coverImage: UIImage
    @Published var assetLoaded: Bool
    

    override init() {
        
        self.year = 1970
        self.month = 1
        self.day = 0
        self.weekOfDay = 1
        self.numberOfImages = 0
        self.defaultCoverAsset_localIdentifier = ""
        self.customCoverAsset_localIdentifier = ""
        self.coverImage = UIImage()
        self.assetLoaded = false
        super.init()
        
    }
    
    init(year:Int, month:Int, day:Int) {
        
        self.year = year
        self.month = month
        self.day = day
        
        var dayComponets = DateComponents()
        dayComponets.year = year
        dayComponets.month = month
        dayComponets.day = day
        let weekOfDay  = Calendar.current.component(.weekday, from: Calendar.current.date(from: dayComponets)! )
        self.weekOfDay = weekOfDay

        self.numberOfImages = 0
        self.defaultCoverAsset_localIdentifier = ""
        self.customCoverAsset_localIdentifier = ""
        self.coverImage = UIImage()
        self.assetLoaded = false

        super.init()
        
    }
    
}



class PhotoFetcher : ObservableObject {
    
    @Published var taskCompleted = false
    
    func askDayItemToPhotoLibrary(calendarID: Int, dayItem:DayItem) {
        
        //DBに登録されているCustom SeetのLocalIdentifierを取得
        //        let beginDate = Date()
        //        let tempLocalIdentifier = RealmManager().getCustomCoverAssetLocalIdentifier( calendarID:calendarID, year:dayItem.year, month:dayItem.month, day:dayItem.day )
        let tempLocalIdentifier = RealmManager.shared.getCustomCoverAssetLocalIdentifier( calendarID:calendarID, year:dayItem.year, month:dayItem.month, day:dayItem.day )
        //        let compDate = Date()
        //        print("Realm 実行時間 : " + (String)(compDate.timeIntervalSince1970 - beginDate.timeIntervalSince1970) )
        
        if tempLocalIdentifier == "" {
            //登録されていない
            //default Assetを取得する
            dayItem.customCoverAsset_localIdentifier = ""
            
        } else if tempLocalIdentifier == "blank" {
            dayItem.customCoverAsset_localIdentifier = tempLocalIdentifier
        } else {
            //登録されている
            //存否確認
            let results = PHAsset.fetchAssets(withLocalIdentifiers: [tempLocalIdentifier], options: nil)
            if ( results.count != 0  ) {
                //存在した
                //customAssetLocalIdentifierの画像を取得する
                dayItem.customCoverAsset_localIdentifier = tempLocalIdentifier
                
            } else {
                //存在しない
                //DBでは、削除された画像がカバーに設定されている。削除する
                //                RealmManager().deleteCoverAsset( calendarID:calendarID, year:dayItem.year, month:dayItem.month, day:dayItem.day )
                RealmManager.shared.deleteCoverAsset( calendarID:calendarID, year:dayItem.year, month:dayItem.month, day:dayItem.day )
                //default Assetを取得する
                dayItem.customCoverAsset_localIdentifier = ""
            }
        }
        
        
        DispatchQueue.global().async {
            
            //defaultとなるAssetのLocalIdentifierを取得
            let assetInfo = AssetManager.shared.getDefaultCoverAssetIdentifier(year: dayItem.year, month: dayItem.month, day: dayItem.day )
            dayItem.defaultCoverAsset_localIdentifier = assetInfo.localIdentifier
            dayItem.numberOfImages = assetInfo.numberOfImages
            
            var targetLocalIdentifier = ""
            if dayItem.customCoverAsset_localIdentifier == "" {
                //カスタムなし
                if dayItem.defaultCoverAsset_localIdentifier == "" {
                    //登録画像なし
                    DispatchQueue.main.async {
                        dayItem.coverImage = UIImage()
                        dayItem.assetLoaded = true
                        self.taskCompleted = true
                    }
                } else {
                    //デフォ画像あり
                    targetLocalIdentifier = dayItem.defaultCoverAsset_localIdentifier
                }
            } else if dayItem.customCoverAsset_localIdentifier == "blank" {
                //カバーなし
                DispatchQueue.main.async {
                    dayItem.coverImage = UIImage()
                    dayItem.assetLoaded = true
                    self.taskCompleted = true
                }
            } else {
                //カスタム設定
                targetLocalIdentifier = dayItem.customCoverAsset_localIdentifier
            }
            
            
            if targetLocalIdentifier != "" {
                
                let results = PHAsset.fetchAssets(withLocalIdentifiers: [targetLocalIdentifier], options: nil)
                if ( results.count != 0  ) {
                    //存在した
                    let phasset = results.firstObject
                    let targetSize = CGSize(width: 256.0, height: 256)
                    PHImageManager.default().requestImage(for: phasset!,
                                                          targetSize: targetSize,
                                                          contentMode: .default,
                                                          options: nil,
                                                          resultHandler: { (image, info) in
                                                            
                                                            DispatchQueue.main.async {
                                                                if image == nil {
                                                                    dayItem.coverImage = UIImage()
                                                                } else {
                                                                    dayItem.coverImage = image!
                                                                }
                                                                dayItem.assetLoaded = true
                                                                self.taskCompleted = true
                                                            }
                                                            
                                                          })
                } else {
                    //存在しない
                    DispatchQueue.main.async {
                        dayItem.coverImage = UIImage()
                        dayItem.assetLoaded = true
                        self.taskCompleted = true
                    }
                    
                }
            }
            
        }
    }
    
    func askImageItemToPhotoLibrary(imageItem:ImageItem) {
        
        DispatchQueue.global().async {
            
            if imageItem.localIdentifier == "" {
                
                imageItem.image = UIImage()
                DispatchQueue.main.async {
                    self.taskCompleted = true
                }
                
            } else {
                
                let results = PHAsset.fetchAssets(withLocalIdentifiers: [imageItem.localIdentifier], options: nil)
                if ( results.count != 0  ) {
                    //存在した
                    let phasset = results.firstObject
                    let targetSize = CGSize(width: 256.0, height: 256)
                    PHImageManager.default().requestImage(for: phasset!,
                                                          targetSize: targetSize,
                                                          contentMode: .default,
                                                          options: nil,
                                                          resultHandler: { (image, info) in
                                                            
                                                            if image == nil {
                                                                imageItem.image = UIImage()
                                                            } else {
                                                                imageItem.image = image!
                                                                imageItem.creationDate = (phasset?.creationDate)!
                                                            }
                                                            DispatchQueue.main.async {
                                                                self.taskCompleted = true
                                                            }
                                                            
                                                          })
                    
                    //動画かどうかのフラグを設定
                    //動画の場合、プレイヤーにロード
                    imageItem.isVideo = (phasset?.mediaType == PHAssetMediaType.video)
                    
                    
                } else {
                    //存在しない
                    imageItem.image = UIImage()
                    DispatchQueue.main.async {
                        self.taskCompleted = true
                    }
                }
            }
        }
    }
}



