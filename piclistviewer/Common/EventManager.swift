//
//  EventManager.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2021/02/05.
//

import Foundation
import AdSupport
import Firebase


class EventManager: NSObject {

    static let shared = EventManager()
    let GBL = Global.shared

    var versionStringForAnalytics: String
    
    private override init() {
        
        //Analyitc用のVersion Stringsを取得
        versionStringForAnalytics = GBL.version ?? "0.0.0"
        //パラメータ値であれば、小数点も使用可能　よって以下はコメントアウトする
        //while let range = versionStringForAnalytics.range(of: ".") {
        //    versionStringForAnalytics.replaceSubrange(range, with: "c")
        //}
        

    }
    

    // analiticsに送る情報 ////////////////////////////////////////////////////////////////////////////////////
    // #46で内容を見直した
    
    func callAnswer_AppLaunched(isPremium:Bool, isTrialMode:Bool) {
        if GBL.isTestBuild {return}
        //analytics 小文字英数字とアンダースコアしかつかえない

        var logStr = ""
        if (isPremium) {
            logStr = "premium"
        } else {
            if (isTrialMode) {
                logStr = "trial"
            } else {
                logStr = "restricted"
            }
        }
        let analyticsDeviceType = String(format: "%d", GBL.screenWidthType)
        
        var UILang = ""
        if (GBL.UILang == 1) {
            UILang = "ja"
        } else {
            UILang = "en"
        }

        Analytics.logEvent("app_launch",
                           parameters: [
                            FirebaseAnalytics.AnalyticsParameterItemCategory: logStr,
                            FirebaseAnalytics.AnalyticsParameterItemCategory2: UILang,
                            FirebaseAnalytics.AnalyticsParameterItemCategory3: GBL.systemVersion,
                            FirebaseAnalytics.AnalyticsParameterItemCategory4: analyticsDeviceType,
                            "version":self.versionStringForAnalytics
                           ])
        
    }

    


    

    func callAnswer_PurchasePremium(isCompleted:Bool) {
        if GBL.isTestBuild {return}
        //analytics 小文字英数字とアンダースコアしかつかえない
        var logStr = ""
        if (isCompleted) {
            logStr = "sccuess"
        } else {
            logStr = "fail"
        }

        let analyticsEventName = "purchased_" + logStr + "_" + self.versionStringForAnalytics

        Analytics.logEvent(analyticsEventName, parameters: nil)

    }
    
    


    //2.0.3で追加
    func callAnswer_commentlog(comment1:String, comment2:String, comment3:String) {
        if GBL.isTestBuild {return}

        Analytics.logEvent("commentlog",
                           parameters: [
                            "comment1":comment1,
                            "comment2":comment2,
                            "comment3":comment3
                           ])

    }
    
    func callAnswer_IDFAHasBeenShown(isCompleted:Bool) {
        if GBL.isTestBuild {return}
        //analytics 小文字英数字とアンダースコアしかつかえない
        var logStr = ""
        if (isCompleted) {
            logStr = "sccuess"
        } else {
            logStr = "fail"
        }

        let analyticsEventName = "idfa_hasbeenshown_" + logStr + "_" + self.versionStringForAnalytics

        Analytics.logEvent(analyticsEventName, parameters: nil)

    }



}

