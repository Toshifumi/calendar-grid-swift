//
//  Common.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2021/01/14.
//

import Foundation
import SwiftUI
import Reachability


class cmn {

    class func calcSelectedFlag(taregetIdentifier: String, dayItem:DayItem) -> Bool {
        
        var retValue = false
        
        if dayItem.customCoverAsset_localIdentifier == "" {
            if dayItem.defaultCoverAsset_localIdentifier == taregetIdentifier {
                retValue = true
            } else {
                retValue = false
            }
        } else if dayItem.customCoverAsset_localIdentifier == taregetIdentifier {
            retValue = true
        } else {
            retValue = false
        }
    
        return retValue
        
    }
    
    class func willShowThisDay(weekOfDay:Int, appCalendarSetting:AppCalendarSetting ) -> Bool {
        
        var retValue = false
        retValue = retValue || (weekOfDay == 1 && appCalendarSetting.SUN)
        retValue = retValue || (weekOfDay == 2 && appCalendarSetting.MON)
        retValue = retValue || (weekOfDay == 3 && appCalendarSetting.TUE)
        retValue = retValue || (weekOfDay == 4 && appCalendarSetting.WED)
        retValue = retValue || (weekOfDay == 5 && appCalendarSetting.THU)
        retValue = retValue || (weekOfDay == 6 && appCalendarSetting.FRI)
        retValue = retValue || (weekOfDay == 7 && appCalendarSetting.SAT)

        return retValue
    }
    
    class func CalendarColor_Base(calendarID:Int) -> Color {
        
        var retValue:Color!
        
        switch calendarID {
        case 0:
            retValue = Color("Calendar0_Base")
        case 1:
            retValue = Color("Calendar1_Base")
        case 2:
            retValue = Color("Calendar2_Base")
        default:
            retValue = Color.gray
        }
        
        return retValue
        
    }

    class func isNetworkreachable() -> Bool  {
        
        let reachability = try! Reachability()
        
        return reachability.connection != .unavailable

    }


    class func showNoNetworkDialog() {
        
        let alert = Alert(title: Text(NSLocalizedString("loc_dialog_noNetwork_title", comment: "")),
                          message: Text(NSLocalizedString("loc_dialog_noNetwork_Body", comment: "")),
                          dismissButton: .default(Text(NSLocalizedString("loc_OK", comment: "")), action: {
                          }))
        
        RootViewManager.shared.alertContent = alert
        RootViewManager.shared.showAlert = true
        
    }


}
