//
//  RootViewManager.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2021/02/10.
//

import Foundation
import SwiftUI


class RootViewManager: NSObject, ObservableObject {
    
    static let shared = RootViewManager()
    
    @Published public var trigger_ProgressView = 0
    public var showProgress: Bool = false

    @Published public var showAlertTrigger:Int = 0
    public var showAlert:Bool = false
    public var alertContent:Alert = Alert(title: Text("Calendar Grid") )

    @Published public var showInstruction_DayView_Trigger:Int = 0
    public var showInstruction_DayView:Bool = false

    @Published public var showInstruction_ImageView_Trigger:Int = 0
    public var showInstruction_ImageView:Bool = false

    override init() {
        super.init()
    }
    
    func setProgress(show:Bool) {
        self.showProgress = show
        self.trigger_ProgressView += 1
    }
    
    
    
}
