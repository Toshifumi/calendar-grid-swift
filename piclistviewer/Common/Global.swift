//
//  Global.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2021/02/05.
//

import Foundation
import SwiftUI

class Global: NSObject {
    
    static let shared = Global()
    
    let version:String!
    let screenWidthType:Int!
    var systemVersion:String!
    let UILang:Int  // 1:Japanese 0:Other is English

    let isTestBuild:Bool!

    let shortWeekDaySymbols:Array<String>!
    let weekDaySymbols:Array<String>!

    override init() {
        
        //version number
        let infoDictionary = Bundle.main.infoDictionary!
        let version_NSStrings:NSString = infoDictionary["CFBundleShortVersionString"]! as! NSString
        version = version_NSStrings as String

        //画面幅
        // https://www.webdlab.com/guide/screen-resolution-3/
        switch UIScreen.main.bounds.size.width {
        case 320 ..< 375:
            //4インチ画面 iPhone5, 5s, SE
            //568 x 320
            screenWidthType = 1
            
        case 375 ..< 414:
            //4.7インチ iPhone6, 6s, 7, 8
            //667 x 375
            //5.8インチ iPhone X, Xs, 11Pro
            //812 x 375
            screenWidthType = 2

        case 414 ..< 420:
            //420はダミー数値
            //5.5インチ 6plus, 7plus, 8plus
            //736 x 414
            //6.1インチ XR, 11
            //896 x 414
            //6.5インチ Xs Max, 11Pro Max
            //896 x 414
            screenWidthType = 3

        default:
            //デバイス判定不可
            //多分iPadはここ
            screenWidthType = 0
        }
        
        //表示言語チェック
        if NSLocale.preferredLanguages.first?.components(separatedBy: "-").first == "ja" {
            UILang = 1
        } else {
            //English -> en
            UILang = 0
        }
        
        //iOSバージョン
        systemVersion = UIDevice.current.systemVersion

        //is Test Build ビルド番号にマイナスがあればテストビルド
        let range: NSRange = version_NSStrings.range(of: "-")
        isTestBuild = (range.location != NSNotFound);
        
        //weekdaySymbol
        let dateFormatter = DateFormatter()
        let language = Locale.preferredLanguages.first!
        let locale = Locale(identifier: language)
        dateFormatter.locale = locale
        self.shortWeekDaySymbols = dateFormatter.shortWeekdaySymbols
        self.weekDaySymbols = dateFormatter.weekdaySymbols
        
        

        super.init()

    }
    
    
    func getShortWeekdaySymbols() -> Array<String> {
        
        let dateFormatter = DateFormatter()
        let language = Locale.preferredLanguages.first!
        let locale = Locale(identifier: language)
        dateFormatter.locale = locale
        
        return dateFormatter.shortWeekdaySymbols
        
    }

    
}
