//
//  RealmManager.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2020/12/17.
//

import Foundation
import RealmSwift

class RealmManager: NSObject {
    

    static let shared = RealmManager()

    let realm:Realm!
//    let realm = try! Realm()

    
    override init() {
        
        // Configurationを生成
        var config = Realm.Configuration()
        config.schemaVersion = 1
        self.realm = try!(Realm(configuration: config))
            
        super.init()

        // デフォルトのファイルを利用する初期化
        try! self.realm.write {
            self.realm.deleteAll()
        }

    }
    
    
    
    public func getCustomCoverAssetLocalIdentifier( calendarID:Int, year:Int, month:Int, day:Int ) -> String {
        
        var retValue: String = ""
        
        //検索
        let results = self.realm.objects(CoverAsset.self).filter(
            "calendarID = " + String(format: "%d", calendarID) +
            " AND year = " + String(format: "%d", year) +
            " AND month = " + String(format: "%d", month) +
            " AND day = " + String(format: "%d", day)
        )

        if results.count == 0 {
            //まだ登録がない
            retValue = ""
        } else {
            //すでに登録があるので更新する
            retValue = results[0].localIdentifier
        }
        
        return retValue
        
    }
    
    public func setCustomCoverAssetLocalIdentifier( calendarID:Int, year:Int, month:Int, day:Int, localIdentifier:String ) {
        
        if localIdentifier == "" {
            //カバーアセットの登録を削除する
            self.deleteCoverAsset(calendarID: calendarID, year: year, month: month, day: day)
        } else {
            //追加、もしくは変更する
            let newCover = CoverAsset()
            newCover.setData( calendarID:calendarID, year:year, month:month, day:day, localIdentifier:localIdentifier )
            //検索
            let results = self.realm.objects(CoverAsset.self).filter(
                "calendarID = " + String(format: "%d", calendarID) +
                " AND year = " + String(format: "%d", year) +
                " AND month = " + String(format: "%d", month) +
                " AND day = " + String(format: "%d", day)
            )

            if results.count == 0 {
                //まだ登録がない
                try! realm.write {
                  realm.add(newCover)
                }
            } else {
                //すでに登録があるので更新する
                try! realm.write {
                    realm.delete(results)
                    realm.add(newCover)
                }
            }

        }
        
    }
    
    
    public func deleteCoverAsset( calendarID:Int, year:Int, month:Int, day:Int ) {
        
        //検索
        let results = self.realm.objects(CoverAsset.self).filter(
            "calendarID = " + String(format: "%d", calendarID) +
            " AND year = " + String(format: "%d", year) +
            " AND month = " + String(format: "%d", month) +
            " AND day = " + String(format: "%d", day)
        )
        
        //データの削除
        if results.count == 0 {
        } else {
            try! realm.write {
                realm.delete(results)
            }
        }
    }
    
}



class CoverAsset: Object {
    
    @objc dynamic var calendarID = -1
    @objc dynamic var year = -1
    @objc dynamic var month = -1
    @objc dynamic var day = -1
    @objc dynamic var localIdentifier =  ""

    public func setData( calendarID:Int, year:Int, month:Int, day:Int, localIdentifier:String ) {
        
        self.calendarID = calendarID
        self.year = year
        self.month = month
        self.day = day
        self.localIdentifier = localIdentifier

//        super.init()
    }


}

