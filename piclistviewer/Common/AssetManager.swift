//
//  AssetManager.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2020/12/16.
//

import Foundation
import SwiftUI
import Photos


class AssetManager: NSObject, PHPhotoLibraryChangeObserver, ObservableObject {
    
    static let shared = AssetManager()
    
    var oldestAssetDate: Date = Date()
    var permissionForPhoto = false
    
    var masterFetchOptions = PHFetchOptions()
    var masterFetchResult: PHFetchResult<PHAsset>!
    
    var ContentChangedDates:Array<Date> = Array()
    @Published var updateTrigger = 0

    
    override init() {
        
        super.init()

        self.refreshAssetManager()
        
        self.checkPreviousVersionFileAndConvert()
        
        PHPhotoLibrary.shared().register(self)
        
//        print("NSHomeDirectory() : ", NSHomeDirectory())
//        self.loadMasrerAssetList()

    }
    
    public func refreshAssetManager() {
        
        self.permissionForPhoto = self.checkPermissionForPhoto()
        
//        if !self.permissionForPhoto {
//            return
//        }

        self.loadMasrerAssetList()
        self.oldestAssetDate = self.getOldestAssetDate()
        
    }
    
    func checkPreviousVersionFileAndConvert() {
        // https://qiita.com/noppefoxwolf/items/f8edb32d435b1894cc4c
        
        if UserDefaults.standard.bool(forKey: "isCheckedOlddata") {return}
        
        let coverData_HomePath = NSHomeDirectory() + "/Documents/Data/"
        let coverData0_path = coverData_HomePath + "CoverAssets0.dat"
        let coverData1_path = coverData_HomePath + "CoverAssets1.dat"
        let coverData2_path = coverData_HomePath + "CoverAssets2.dat"

        if FileManager.default.fileExists(atPath: coverData0_path) {
            //ファイルが存在
            let covers0 = NSKeyedUnarchiver.unarchiveObject(withFile: coverData0_path) as! Array<WBTabCoverAsset>
            if covers0.count != 0 {
                for cover in covers0 {
//                    print("contents of cover : ", cover.year, cover.month, cover.day, cover.localIdentifier)
                    RealmManager.shared.setCustomCoverAssetLocalIdentifier( calendarID:0, year:cover.year, month:cover.month, day:cover.day, localIdentifier: cover.localIdentifier as String )
                }
            }
        }

        if FileManager.default.fileExists(atPath: coverData1_path) {
            //ファイルが存在
            let covers1 = NSKeyedUnarchiver.unarchiveObject(withFile: coverData1_path) as! Array<WBTabCoverAsset>
            if covers1.count != 0 {
                for cover in covers1 {
//                    print("contents of cover : ", cover.year, cover.month, cover.day, cover.localIdentifier)
                    RealmManager.shared.setCustomCoverAssetLocalIdentifier( calendarID:1, year:cover.year, month:cover.month, day:cover.day, localIdentifier: cover.localIdentifier as String )
                }
            }
        }

        if FileManager.default.fileExists(atPath: coverData2_path) {
            //ファイルが存在
            let covers2 = NSKeyedUnarchiver.unarchiveObject(withFile: coverData2_path) as! Array<WBTabCoverAsset>
            if covers2.count != 0 {
                for cover in covers2 {
//                    print("contents of cover : ", cover.year, cover.month, cover.day, cover.localIdentifier)
                    RealmManager.shared.setCustomCoverAssetLocalIdentifier( calendarID:2, year:cover.year, month:cover.month, day:cover.day, localIdentifier: cover.localIdentifier as String )
                }
            }
        }

        UserDefaults.standard.set(true, forKey: "isCheckedOlddata")
        
    }
    
    

    public func photoLibraryDidChange(_ changeInstance: PHChange) {
        
        var ResultDates: Array<Date> = Array()

        if let resultDetailChanges = changeInstance.changeDetails(for: self.masterFetchResult) {
            
            if resultDetailChanges.changedObjects.count != 0 {
                resultDetailChanges.changedObjects.forEach { (asset) in
//                    print("changedObjects asset.creationDate?.description ", asset.creationDate?.description)
                    ResultDates.append(asset.creationDate!)
                }
            }

            if resultDetailChanges.insertedObjects.count != 0 {
                resultDetailChanges.insertedObjects.forEach { (asset) in
//                    print("insertedObjects asset.creationDate?.description ", asset.creationDate?.description)
                    ResultDates.append(asset.creationDate!)
                }
            }

            if resultDetailChanges.removedObjects.count != 0 {
                resultDetailChanges.removedObjects.forEach { (asset) in
//                    print("removedObjects asset.creationDate?.description ", asset.creationDate?.description)
                    ResultDates.append(asset.creationDate!)
                }
            }

            if resultDetailChanges.hasIncrementalChanges && (ResultDates.count != 0){
                DispatchQueue.main.async {
                    self.ContentChangedDates = ResultDates
//                    print("ResultDates.count : ", ResultDates.count )
                    self.updateTrigger += 1
                }
            }
            
            self.loadMasrerAssetList()

//            if ResultDates.count != 0 {
//                DispatchQueue.main.async {
//                    self.ContentChangedDates = ResultDates
//                    print("ResultDates.count : ", ResultDates.count )
//                    self.updateTrigger += 1
//
//                }
//            }
        }
    }
    
    
    
    
    
    private func checkPermissionForPhoto() -> Bool {
        
        var retValue = false
        
        if PHPhotoLibrary.authorizationStatus() == .authorized {
            //Permissionあり
            retValue = true
        }
        
        return retValue
        
    }
    
    func loadMasrerAssetList() {
        
        // let begin = Date()
        self.masterFetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
        self.masterFetchResult = PHAsset.fetchAssets(with: masterFetchOptions)  //was new_fetchResult
        // let finish = Date()
        // print("Time : " + (String)(finish.timeIntervalSince1970 - begin.timeIntervalSince1970) )
        
    }
    
    func getOldestAssetDate() -> Date{

        // let begin = Date()

        var retValue = Date()
        
        if (self.masterFetchResult.count != 0) {
            retValue = (self.masterFetchResult.firstObject?.creationDate)!
        }

        // let finish = Date()
        // print("getOldestAssetDate Time : " + (String)(finish.timeIntervalSince1970 - begin.timeIntervalSince1970) )
        // print("retValue ", retValue.description)

        return retValue
        
    }
    
    
    
    func getDefaultCoverAssetIdentifier( year:Int, month:Int, day:Int) -> ImagesInfoForDay {
        
        let retValue:ImagesInfoForDay = ImagesInfoForDay()
        
        var localCalendar = Calendar.current
        localCalendar.firstWeekday = 1 // 日曜日を開始曜日
        localCalendar.minimumDaysInFirstWeek = 4 //初週の設定をISO方式(1/4を含む週)に設定
        
        //月初日の曜日
        //指定した日付が何曜日か取得（1=日曜日〜7=土曜日）
        // 年月日をとりだす
        var begining_dateComponents = DateComponents()
        begining_dateComponents.year = year
        begining_dateComponents.month = month
        begining_dateComponents.day = day
        begining_dateComponents.hour = 0
        begining_dateComponents.minute = 0
        begining_dateComponents.second = 0
        let theDay_Date:Date = localCalendar.date(from: begining_dateComponents)!
        
        //次の終わり
        let nextDay_Date:Date = localCalendar.date(byAdding: .day, value: 1, to: theDay_Date)!
        
        //一覧の取得
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
        fetchOptions.predicate = NSPredicate(format: "creationDate >= %@ && creationDate < %@", theDay_Date as NSDate, nextDay_Date as NSDate)
        let fetchResult = PHAsset.fetchAssets(with: fetchOptions)  //was new_fetchResult
        
        //        print("Start checkinhg : " +  (String)(fetchResult.count) )
        //        let begin = Date()
        if (fetchResult.count != 0) {
            retValue.localIdentifier = fetchResult.firstObject!.localIdentifier
            retValue.numberOfImages = fetchResult.count
        }
        //        let finish = Date()
        //        print("Finish checkinhg")
        //        print("Time : " + (String)(finish.timeIntervalSince1970 - begin.timeIntervalSince1970) )
        
        return retValue
        
    }
    
    
    func getLocalIdentifierArrayForTheDay(year:Int, month:Int, day:Int) -> Array<String> {
        
        var retValue:Array<String> = Array()
        
        let localCalendar = Calendar.current
        //月初日の曜日
        //指定した日付が何曜日か取得（1=日曜日〜7=土曜日）
        // 年月日をとりだす
        var begining_dateComponents = DateComponents()
        begining_dateComponents.year = year
        begining_dateComponents.month = month
        begining_dateComponents.day = day
        begining_dateComponents.hour = 0
        begining_dateComponents.minute = 0
        begining_dateComponents.second = 0
        let theDay_Date:Date = localCalendar.date(from: begining_dateComponents)!
        
        //次の終わり
        let nextDay_Date:Date = localCalendar.date(byAdding: .day, value: 1, to: theDay_Date)!
        
        //一覧の取得
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
        fetchOptions.predicate = NSPredicate(format: "creationDate >= %@ && creationDate < %@", theDay_Date as NSDate, nextDay_Date as NSDate)
        let fetchResult = PHAsset.fetchAssets(with: fetchOptions)  //was new_fetchResult
        if (fetchResult.count == 0) {
            //データなし
        } else {
            //データあり
            for i in 0..<fetchResult.count {
                let item = fetchResult[i]
                retValue.append(item.localIdentifier)
            }
        }
        
        return retValue
        
    }
    
    func isExistPhotoAtThisMonth2(year:Int, month:Int, appCalendarSetting:AppCalendarSetting ) -> Bool {
        
        var retValue = false
        
        let localCalendar = Calendar.current
        //月初日の曜日
        //指定した日付が何曜日か取得（1=日曜日〜7=土曜日）
        // 年月日をとりだす
        var begining_dateComponents = DateComponents()
        begining_dateComponents.year = year
        begining_dateComponents.month = month
        begining_dateComponents.day = 1
        begining_dateComponents.hour = 0
        begining_dateComponents.minute = 0
        begining_dateComponents.second = 0
        let firstDayOfTheMonth_Date:Date = localCalendar.date(from: begining_dateComponents)!
        
        //次の月の初日
        let firstDayOfNextMonth_Date:Date = localCalendar.date(byAdding: .month, value: 1, to: firstDayOfTheMonth_Date)!
        
        //一覧の取得
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
        fetchOptions.predicate = NSPredicate(format: "creationDate >= %@ && creationDate < %@", firstDayOfTheMonth_Date as NSDate, firstDayOfNextMonth_Date as NSDate)
        let fetchResult = PHAsset.fetchAssets(with: fetchOptions)  //was new_fetchResult
        if (fetchResult.count == 0) {
            //データなし
            retValue = false
        } else {
            //データあり
            let localCalendar = Calendar.current
            fetchResult.enumerateObjects({ asset, index, stop in
                
                let creationDate = (asset.creationDate)!
                let creationDate_DayComponets = localCalendar.dateComponents(in: TimeZone.current, from: creationDate )

                if cmn.willShowThisDay(weekOfDay:creationDate_DayComponets.weekday!, appCalendarSetting:appCalendarSetting ) {
                    retValue = true
                    stop.pointee = true
                }
                
            })
        }
        
        return retValue
        
    }
    
    func isExistPhotoAtThisMonth(year:Int, month:Int) -> Bool {
        
        var retValue = false
        
        let localCalendar = Calendar.current
        //月初日の曜日
        //指定した日付が何曜日か取得（1=日曜日〜7=土曜日）
        // 年月日をとりだす
        var begining_dateComponents = DateComponents()
        begining_dateComponents.year = year
        begining_dateComponents.month = month
        begining_dateComponents.day = 1
        begining_dateComponents.hour = 0
        begining_dateComponents.minute = 0
        begining_dateComponents.second = 0
        let firstDayOfTheMonth_Date:Date = localCalendar.date(from: begining_dateComponents)!
        
        //次の月の初日
        let firstDayOfNextMonth_Date:Date = localCalendar.date(byAdding: .month, value: 1, to: firstDayOfTheMonth_Date)!
        
        //一覧の取得
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
        fetchOptions.predicate = NSPredicate(format: "creationDate >= %@ && creationDate < %@", firstDayOfTheMonth_Date as NSDate, firstDayOfNextMonth_Date as NSDate)
        fetchOptions.fetchLimit = 1
        let fetchResult = PHAsset.fetchAssets(with: fetchOptions)  //was new_fetchResult
        if (fetchResult.count == 0) {
            //データなし
            retValue = false
        } else {
            //データあり
            retValue = true
        }
        
        return retValue
        
    }
    
    
    func isExistPhotoAtThisDay(year:Int, month:Int, day:Int) -> Bool {
        
        var retValue = false
        
        let localCalendar = Calendar.current
        //月初日の曜日
        //指定した日付が何曜日か取得（1=日曜日〜7=土曜日）
        // 年月日をとりだす
        var begining_dateComponents = DateComponents()
        begining_dateComponents.year = year
        begining_dateComponents.month = month
        begining_dateComponents.day = day
        begining_dateComponents.hour = 0
        begining_dateComponents.minute = 0
        begining_dateComponents.second = 0
        let theDay:Date = localCalendar.date(from: begining_dateComponents)!
        
        //一日の終わり
        let nextDay:Date = localCalendar.date(byAdding: .day, value: 1, to: theDay)!
        
        //一覧の取得
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
        fetchOptions.predicate = NSPredicate(format: "creationDate >= %@ && creationDate < %@", theDay as NSDate, nextDay as NSDate)
        fetchOptions.fetchLimit = 1
        let fetchResult = PHAsset.fetchAssets(with: fetchOptions)  //was new_fetchResult
        if (fetchResult.count == 0) {
            //データなし
            retValue = false
        } else {
            //データあり
            retValue = true
        }
        
        return retValue
        
    }
    
    
    func getBeforeDayItemOf(targetDayItem:DayItem, appCalendarSetting:AppCalendarSetting) -> DayItem! {
        //日付の情報だけが入ったDayItemを返す
        
        var retValue:DayItem! = nil
        
        let localCalendar = Calendar.current
        //月初日の曜日
        //指定した日付が何曜日か取得（1=日曜日〜7=土曜日）
        // 年月日をとりだす
        var targetDay_dateComponents = DateComponents()
        targetDay_dateComponents.year = targetDayItem.year
        targetDay_dateComponents.month = targetDayItem.month
        targetDay_dateComponents.day = targetDayItem.day
        var targetDay_Date = localCalendar.date(from: targetDay_dateComponents)!
        
        while (oldestAssetDate < targetDay_Date) {
            //一覧の取得
            let fetchOptions = PHFetchOptions()
            fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
            fetchOptions.predicate = NSPredicate(format: "creationDate < %@", targetDay_Date as NSDate)
            fetchOptions.fetchLimit = 1
            
            let fetchResult = PHAsset.fetchAssets(with: fetchOptions)  //was new_fetchResult
            
            if (fetchResult.count == 0) {
                //データなし nilのまま返す
                break
            } else {
                //データあり
                let beforeDay_Date = (fetchResult.firstObject?.creationDate)!
                let beforeDay_DayComponets = localCalendar.dateComponents(in: TimeZone.current, from: beforeDay_Date )
                
                
                if cmn.willShowThisDay(weekOfDay:beforeDay_DayComponets.weekday!, appCalendarSetting:appCalendarSetting ) {
                    retValue = DayItem(year: beforeDay_DayComponets.year!, month: beforeDay_DayComponets.month!, day: beforeDay_DayComponets.day!)
                    break
                } else {
                    targetDay_dateComponents.year = beforeDay_DayComponets.year
                    targetDay_dateComponents.month = beforeDay_DayComponets.month
                    targetDay_dateComponents.day = beforeDay_DayComponets.day
                    targetDay_Date = localCalendar.date(from: targetDay_dateComponents)!
                }
                
            }
        }
        
        return retValue
        
    }
    
    
    func getAfterDayItemOf(targetDayItem:DayItem, appCalendarSetting:AppCalendarSetting) -> DayItem! {
        
//        getBeforeDayItemOfと同じ実装をする
        //日付の情報だけが入ったDayItemを返す
        var retValue:DayItem! = nil
        
        let today = Date()
        
        let localCalendar = Calendar.current
        //月初日の曜日
        //指定した日付が何曜日か取得（1=日曜日〜7=土曜日）
        // 年月日をとりだす
        var targetDay_dateComponents = DateComponents()
        targetDay_dateComponents.year = targetDayItem.year
        targetDay_dateComponents.month = targetDayItem.month
        targetDay_dateComponents.day = targetDayItem.day + 1
        var targetDay_Date:Date = localCalendar.date(from: targetDay_dateComponents)!
        
        while (targetDay_Date < today) {
            //一覧の取得
            let fetchOptions = PHFetchOptions()
            fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
            fetchOptions.predicate = NSPredicate(format: "%@ <= creationDate", targetDay_Date as NSDate)
            fetchOptions.fetchLimit = 1
            
            let fetchResult = PHAsset.fetchAssets(with: fetchOptions)  //was new_fetchResult
            
            if (fetchResult.count == 0) {
                //データなし nilのまま返す
                break
            } else {
                //データあり
                let afterDay_Date = (fetchResult.firstObject?.creationDate)!
                let afterDay_DayComponets = localCalendar.dateComponents(in: TimeZone.current, from: afterDay_Date )
                
                if cmn.willShowThisDay(weekOfDay:afterDay_DayComponets.weekday!, appCalendarSetting:appCalendarSetting ) {
                    retValue = DayItem(year: afterDay_DayComponets.year!, month: afterDay_DayComponets.month!, day: afterDay_DayComponets.day!)
                    break
                } else {
                    targetDay_dateComponents.year = afterDay_DayComponets.year
                    targetDay_dateComponents.month = afterDay_DayComponets.month
                    targetDay_dateComponents.day = afterDay_DayComponets.day! + 1
                    targetDay_Date = localCalendar.date(from: targetDay_dateComponents)!
                }
            }
        }

        
        return retValue
        
    }
    
    
    func saveNewAsset(image: UIImage, completion: @escaping (String) -> ()) {
        //https://stackoverflow.com/questions/30401439/how-could-i-create-a-function-with-a-completion-handler-in-swift
        
        var retValue:String! = ""
        
        PHPhotoLibrary.shared().performChanges({
            
            let request = PHAssetCreationRequest.creationRequestForAsset(from: image)
            retValue = request.placeholderForCreatedAsset!.localIdentifier
            
        }, completionHandler: {success, error in
            if success {
                //保存が成功
                completion(retValue)
            } else {
                completion("")
            }

        })


    }
    
    
    
    
    func updateAsset(imageItem: ImageItem, calendarID:Int, dayItem: DayItem, image:UIImage) {
        //https://dev.classmethod.jp/references/ios8-photo-kit-4/
        // https://forums.xamarin.com/discussion/163291/save-changed-image-back-to-photo-library-on-ios-overwrite-original
        
        
        let results = PHAsset.fetchAssets(withLocalIdentifiers: [imageItem.localIdentifier], options: nil)
        if ( results.count != 0  ) {
            //存在した
            let phasset = results.firstObject!
            
            if (phasset.canPerform(PHAssetEditOperation.content)) {
                
                // Prepare the options to pass when requesting to edit the image.
                let options = PHContentEditingInputRequestOptions()
                options.canHandleAdjustmentData = {(adjustmetData: PHAdjustmentData) -> Bool in
                    return false
                }
                
                phasset.requestContentEditingInput(with: options) {contentEditingInput, requestStatusInfo in
                    
                    let adjustmentData = PHAdjustmentData(formatIdentifier: "AdjustmentFormatIdentifier", formatVersion: "1.0", data: "filterName".data(using: String.Encoding.utf8)!)
                    let contentEditingOutput = PHContentEditingOutput(contentEditingInput: contentEditingInput!)
                    let jpegData = image.jpegData(compressionQuality: 0.9)
                    
                    try? jpegData!.write(to: contentEditingOutput.renderedContentURL)
                    contentEditingOutput.adjustmentData = adjustmentData
                    
                    // Ask the shared PHPhotoLinrary to perform the changes.
                    PHPhotoLibrary.shared().performChanges({
                        let request = PHAssetChangeRequest(for: phasset)
                        request.contentEditingOutput = contentEditingOutput
                        
                    }, completionHandler: {success, error in
                        if success {
                        } else {
                            //print("Error: %@", error!) #2
                        }
                    })
                    
                }
            }
        }
    }
    
    
    func deleteAsset(imageItem: ImageItem, calendarID:Int, dayItem: DayItem, completion: @escaping (Bool) -> ()) {
        //https://stackoverflow.com/questions/30401439/how-could-i-create-a-function-with-a-completion-handler-in-swift
        
        
        if imageItem.localIdentifier == "" {
            
            completion(false)
            
        } else {
            
            let results = PHAsset.fetchAssets(withLocalIdentifiers: [imageItem.localIdentifier], options: nil)
            if ( results.count != 0  ) {
                //存在した
                let phasset = results.firstObject
                
                if !phasset!.canPerform(.delete) {
                    completion(false)
                } else {
                    
                    PHPhotoLibrary.shared().performChanges({
                        PHAssetChangeRequest.deleteAssets(NSArray(array: [phasset!]))
                    }, completionHandler: { success, _ in
                        // 削除後の処理
                        if success {
                            DispatchQueue.main.async {
                                //dayItemの更新はphotoLibraryDidChangeから呼ばれるMainViewのrefreshTargetDayItemから行われるので、ここで行う必要はない
                                // was code is PhotoFetcher().askDayItemToPhotoLibrary(calendarID: calendarID, dayItem: dayItem)
                                completion(true)
                            }
                        } else {
                            completion(false)
                        }
                        //削除が行われたので、dayItemの再構築
                    })
                    
                }
                
            } else {
                //存在しない
                completion(false)
            }
        }
    }
}




