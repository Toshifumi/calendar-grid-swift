//
//  LicenseManager.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2021/02/05.
//

import Foundation
import SwiftUI

import UIKit
import StoreKit
import GoogleMobileAds
import ADFMovieReward
import FirebaseRemoteConfig


@objc protocol LicenseManagerDelegate {
    @objc func wallAnswer(key:String, ReadyToGo:Bool)
}

class LicenseManager: NSObject, ObservableObject, IAPManagerDelegate, ADFmyMovieRewardDelegate {
    
    static let shared = LicenseManager()
    
    weak var delegate: LicenseManagerDelegate?
    
    private let remoteConfig = RemoteConfig.remoteConfig()
    
    public var adProvider = 1 // 0: disable 1:Admob, adfurikun
    
    //admob IDs
    static let AdMob_appID = "ca-app-pub-2549412375286123~2312658690"
    public var AdMob_bannerUnitID: String = ""
    let AdMob_bannerUnitID_Real = "ca-app-pub-2549412375286123/9333652664"  //new ID for swiftui version at 2021/2/5
    let AdMob_bannerUnitID_Test = "ca-app-pub-3940256099942544/6300978111"  //public ID for testing
    public var AdMob_intersUnitID: String = ""
    let AdMob_intersUnitID_Real = "ca-app-pub-2549412375286123/9348952775"  //new ID for swiftui version at 2021/2/5
    let AdMob_intersUnitID_Test = "ca-app-pub-3940256099942544/1033173712"  //public ID for testing
    
    
    //adfurikun IDs
    public let adfurikun_banner_APPID = "601ca18942f084d220000047"
    public let adfurikun_interstitial_APPID = "601ca1fd43f084852200000a"
    
    
    let GBL = Global.shared
    let IAPMan = IAPManager.shared
    let EVM = EventManager.shared
//    let RVM = RootViewManager.shared
    
    var isTrialMode = false
    @Published var isPurchased = false
    @Published var updateTrigger = 0

    let productIdentifier = "com.typeddesign.piclistviewer.unlock01"

    public var premiumPrice = ""
    
    //admob
    var admob_interstitial: GADInterstitial!
    var admob_rewardedAd: GADRewardedAd?
    
    //adfurikun
    var ADF_interstitial: ADFmyInterstitial?
    
    //keywaord
    var keyForReward:String = ""
    
    //CalendarGritと同じく、起動時に価格をロードし、購入画面に表示する流れにする
    
    
    private override init() {
        
        super.init()
        
        //無料版、有料版の2段階にする。
        checkTrialMode()
        
        //Admobのuser ID
        
        #if targetEnvironment(simulator)
        
        AdMob_bannerUnitID = AdMob_bannerUnitID_Test
        AdMob_intersUnitID = AdMob_intersUnitID_Test
        
        #else
        
        if Global.shared.isTestBuild {
            AdMob_bannerUnitID = AdMob_bannerUnitID_Test
            AdMob_intersUnitID = AdMob_intersUnitID_Test
        } else {
            AdMob_bannerUnitID = AdMob_bannerUnitID_Real
            AdMob_intersUnitID = AdMob_intersUnitID_Real
        }
        
        #endif
        
        
        //IAP
        IAPMan.delegate = self
        
        //UserDefaultsで購入済みかどうかの一時判断
        isPurchased = UserDefaults.standard.bool(forKey: "isPurchased")
        if Global.shared.isTestBuild {
//            isPurchased = true
//                        isPurchased = false
            //            isTrialMode = false; //無料版 制限期間
            //                        UserDefaults.standard.set(isPurchased, forKey: "isPurchased")  //実機に購入済みフラグを当てるためのダミーｄ
        }
        
        //以下テスト用ダミーコード
//        isTrialMode = true; //無料版 体験期間
//        isTrialMode = false; //無料版 制限期間
//        isPurchased = true
//        isPurchased = false
//                UserDefaults.standard.set(isPurchased, forKey: "isPurchased")  //実機に購入済みフラグを当てるためのダミーｄ
        
        
        //ad設定
        // userDefaultsに初期値を登録する」
        let userDefaults = UserDefaults.standard
        userDefaults.register(defaults: ["ad_provider" : "1"])
        //保存されている設定を呼び出し
        self.adProvider = userDefaults.integer(forKey: "ad_provider")
        
//        if isPurchased {
//            //広告表示しない
//        } else {
//            self.setupAdfeatures()
//        }
//
        self.loadRemoteConfig();
//
        self.checkPremiumPrice()  //#6
        
        //Fabric
        EVM.callAnswer_AppLaunched(isPremium:isPurchased, isTrialMode:isTrialMode)
        
    }
    
    private func loadRemoteConfig() {

        //Firebase remote config : Remote configで設定されているad_providerを読み込む
        let settings = RemoteConfigSettings()
        settings.minimumFetchInterval = 0
        remoteConfig.configSettings = settings

        remoteConfig.fetch() { (status, error) -> Void in
            if status == .success {
                print("Config fetched!")
                self.remoteConfig.activate() { (changed, error) in
                    //remote configで取得したadproviderの設定値をuserDefaultsに記録
                    //UIの更新をともなうので、メインスレッドで実行
                    DispatchQueue.main.async {
                        //adprovider
                        let remoteConfigValue:String = self.remoteConfig["ad_provider"].stringValue ?? "0"
                        self.adProvider = Int(remoteConfigValue) ?? 0
                        UserDefaults.standard.set(self.adProvider, forKey: "ad_provider")
//                        self.setupAdfeatures()

                        //notification //#48
                        let remotecongig_latestVersionOnStore:String = self.remoteConfig["latest_version_onstore"].stringValue ?? "0"
                        let remotecongig_latestNotif_ID:String = self.remoteConfig["latest_notif_ID"].stringValue ?? "0"
                        let remotecongig_latestNotif_MessageJP:String = self.remoteConfig["latest_notif_message_jp"].stringValue ?? "0"
                        let remotecongig_latestNotif_MessageEN:String = self.remoteConfig["latest_notif_message_en"].stringValue ?? "0"

                        if (remotecongig_latestVersionOnStore > self.GBL.version) {
                            //新しいバージョン番号が公開されたとき
                            //このダイアログは非表示にせず、アップデートするまで表示し続けるものとする

                            let alert = Alert(title: Text(NSLocalizedString("loc_AlertAction_NewVersionExist_Title", comment: "")),
                                              message: Text(String(format: NSLocalizedString("loc_AlertAction_NewVersionExist_Message", comment: ""), remotecongig_latestVersionOnStore)),
                                              primaryButton: .default(Text(NSLocalizedString("loc_AlertAction_ShowAppPage", comment: "")), action: {
                                                var appURL:URL
                                                if self.GBL.UILang == 1 {
                                                    appURL = URL(string: "https://apps.apple.com/jp/app/apple-store/id914442503")!
                                                } else {
                                                    appURL = URL(string: "https://apps.apple.com/en/app/apple-store/id914442503")!
                                                }
                                                // URLを開けるかをチェックする
                                                if UIApplication.shared.canOpenURL(appURL) {
                                                    // URLを開く
                                                    UIApplication.shared.open(appURL, options: [:]) { success in
                                                    }
                                                }

                                              }),
                                              secondaryButton: .destructive(Text(NSLocalizedString("loc_AlertAction_ContinueToUse", comment: "")), action: {
                                              }))

                            RootViewManager.shared.alertContent = alert
                            RootViewManager.shared.showAlert = true
                            RootViewManager.shared.showAlertTrigger += 1

                        } else {
                            //現在が最新バージョンのとき、Notifを確認し、新しいのがあれば表示する
                            // 2021.2.15現在、三つボタンのアラートはSWIFTUIでは作れない(自作ならできるけど)
                            // なので、この独自通知を出す機能は Calender Gridでは当面利用しないこととする

//                            let localNotifNumber = UserDefaults.standard.integer(forKey: "notifNumber")
//                            let remotecongig_latestNotif_ID_Int: Int = Int(remotecongig_latestNotif_ID) ?? 0
//
//                            if ( remotecongig_latestNotif_ID_Int > localNotifNumber ) {
//                                //resultのほうが大きい場合 -> net上の情報のほうが新しい
//
//                                // #1
//                                //#1の追加修正 コンテキストの指定をmGlobal.MainActivityContextにしていたためCrashしていた
//                                //コンストラクタで渡されるcontextに変更することで解決
//
//                                var messageText = ""
//                                if (self.GBL.UILang == 1) {
//                                    messageText = remotecongig_latestNotif_MessageJP;
//                                } else {
//                                    messageText = remotecongig_latestNotif_MessageEN;
//                                }
//
//
//                                let alert = Alert(title: Text(NSLocalizedString("loc_AlertAction_NewNotification_Title", comment: "")),
//                                                  message: Text(messageText),
//                                                  primaryButton: .default(Text(NSLocalizedString("loc_OK", comment: "")), action: {
//                                                  }),
//                                                  secondaryButton: .destructive(Text(NSLocalizedString("loc_AlertAction_NoMoreThisNotification", comment: "")), action: {
//                                                    UserDefaults.standard.set(remotecongig_latestNotif_ID_Int, forKey: "notifNumber")
//                                                  }),
//                                                  secondaryButton: .destructive(Text(NSLocalizedString("loc_dialogItem_kickHomePage", comment: "")), action: {
//                                                    let GBL = Global.shared
//                                                    let urlString = GBL.UILang == 1 ? GBL.appHomePage_BLOG_JP : GBL.appHomePage_BLOG_EN
//                                                    UIApplication.shared.open(URL(string: urlString)!, options: [:], completionHandler: nil)
//                                                  }))
//
//
//                                RootViewManager.shared.alertContent = alert
//                                RootViewManager.shared.showAlert = true
//
//
//
//                            } else {
//                                //同じID 同じ情報か、
//                                //net上の情報のほうが古い
//                            }
                        }

                    }
                }
            } else {
            }
        }

    }
    
    private func setupAdfeatures() {

        if adProvider == 2 {
            //adfurikun

            ADFmyInterstitial.initialize(withAppID: adfurikun_interstitial_APPID)
            ADF_interstitial = ADFmyInterstitial.getInstance(adfurikun_interstitial_APPID, delegate: self)
            ADF_interstitial?.load()

        } else if adProvider == 1 {
            //admob
            self.admob_CreateInterstitial()
        } else {
            //disable
        }


    }

    //adfurikun delegate
    //広告ロード成功
    func adsFetchCompleted(_ appID: String, isTestMode isTestMode_inApp: Bool) {
    }
    //広告ロード失敗
    func adsFetchFailed(_ appID: String, error: Error?) {
    }

    //広告表示開始時に呼ばれます。
    func adsDidShow(_ appID: String!, adNetworkKey: String!) {
    }
    //広告表示失敗時に呼ばれます。
    func adsPlayFailed(_ appID: String!) {
    }
    //広告を最後まで視聴した時に呼ばれます。
    func adsDidCompleteShow(_ appID: String!) {
    }
    //広告を閉じた時に呼ばれます。
    func adsDidHide(_ appID: String!) {
    }



    private func checkTrialMode() {

        //初回利用日付が存在しない場合、記録する。
        let appStartedDate:Double = UserDefaults.standard.double(forKey: "appStartedDate")
        if appStartedDate == 0 {
            //0と言うことは、初回利用日がセットされていない。セットする
            UserDefaults.standard.set(Date().timeIntervalSince1970, forKey: "appStartedDate")
            isTrialMode = true; //無料版体験期間

        } else {
            //何らかの日付がセットされている
            //現在までの期間を計算し、30日経過後から、制限期間とする
            let dayInterval = Calendar.current.dateComponents([.day], from: Date(timeIntervalSince1970: appStartedDate), to: Date()).day

            if dayInterval! > 14 {
                //初回使用時から31以上 制限期間
                isTrialMode = false; //無料版 制限期間

            } else {
                isTrialMode = true; //無料版 体験期間
            }
        }

        if GBL.isTestBuild {
//                        isTrialMode = false; //無料版 制限期間
        }

    }

    func needAlertForAdDisplay() -> Bool {
        if self.isPurchased {
            return false
        } else {
            if self.isTrialMode {
                return false
            } else {
                let hideTrialEndedDialog = UserDefaults.standard.bool(forKey: "noMoreTrialEndedDialog")
                if hideTrialEndedDialog {
                    return false
                } else {
                    return true
                }
            }
        }
    }


    //#6
    func checkPremiumPrice() {

        IAPMan.validateProductIdentifiers(productIdentifiers: [productIdentifier]) { [unowned self] products in

            let targetProduct: SKProduct? = {
                return products.filter { $0.productIdentifier == self.productIdentifier }.first
            }()

            // フォーマット
            let numberFmt = NumberFormatter()
            numberFmt.formatterBehavior = .behavior10_4
            numberFmt.numberStyle = .currency
            numberFmt.locale = targetProduct?.priceLocale

            self.premiumPrice = numberFmt.string(from: targetProduct?.price ?? 0) ?? ""

            print("premiumPrice, ", self.premiumPrice)
            

        }

    }

    

    

    func purchase() {
        // http://kray.jp/blog/iphone-in-app-purchase-2/

        //通信可否の確認
        if ( cmn.isNetworkreachable() ) {
        } else {
            //not network connetion
            cmn.showNoNetworkDialog()
            return
        }

        RootViewManager.shared.showProgress = true

        if isPurchased {
            //すでに購入済み
        } else {
        }
        // アプリ内課金が許可されているかを確認
        if (IAPMan.canMakePayments()) {
            //購入可能
            print("allowed to purchase")

        } else {
            //購入できない
            let alert = Alert(title: Text(NSLocalizedString("loc_AlertAction_NeedPurchasable_Title", comment: "")),
                              message: Text(NSLocalizedString("loc_AlertAction_NeedPurchasable_Message", comment: "")),
                              dismissButton: .default(Text(NSLocalizedString("loc_OK", comment: "")), action: {
                                RootViewManager.shared.showProgress = false
                              }))
            
            RootViewManager.shared.alertContent = alert
            RootViewManager.shared.showAlert = true
            RootViewManager.shared.showAlertTrigger += 1

            return
        }

        //通信可否の確認
        if ( cmn.isNetworkreachable() ) {
        } else {
            //not network connetion
            cmn.showNoNetworkDialog()
            return
        }

        SKPaymentQueue.default().add(IAPMan)

        //レシートの復元を行う
        IAPMan.restore()

    }


    func iapManagerDidFinishPurchased() {
        //購入完了をユーザに知らせるアラートを表示
        //Indicatorを隠す処理

        isPurchased = true
        UserDefaults.standard.set(isPurchased, forKey: "isPurchased")
        EVM.callAnswer_PurchasePremium(isCompleted:true)
        
        let alert = Alert(title: Text(NSLocalizedString("loc_AlertAction_PurchaseComplete_Title", comment: "")),
                          message: Text(NSLocalizedString("loc_AlertAction_PurchaseComplete_Message", comment: "")),
                          dismissButton: .default(Text(NSLocalizedString("loc_OK", comment: "")), action: {
                            RootViewManager.shared.showProgress = false
//                            RootViewManager.shared.updateBannerViewStatus()
                          }))
        
        RootViewManager.shared.alertContent = alert
        RootViewManager.shared.showAlert = true
        RootViewManager.shared.showAlertTrigger += 1
        self.updateTrigger += 1

    }
    //購入に失敗した時
    func iapManagerDidFailedPurchased() {
        //購入失敗をユーザに知らせるアラート
        EVM.callAnswer_PurchasePremium(isCompleted:false)

        self.showFailedDialog()
    }
    //リストアが完了した時
    func iapManagerDidFinishRestore(_ productIdentifiers: [String]) {

        var isPurchasedBefore = false
        for identifier in productIdentifiers {
            if identifier == productIdentifier {
                isPurchasedBefore = true
            }
        }
        //Indicatorを隠す処理

        if isPurchasedBefore {
            //購入履歴あり

            isPurchased = true
            UserDefaults.standard.set(isPurchased, forKey: "isPurchased")

            let alert = Alert(title: Text(NSLocalizedString("loc_AlertAction_RestoreComplete_Title", comment: "")),
                              message: Text(NSLocalizedString("loc_AlertAction_RestoreComplete_Message", comment: "")),
                              dismissButton: .default(Text(NSLocalizedString("loc_OK", comment: "")), action: {
                                RootViewManager.shared.showProgress = false
//                                RootViewManager.shared.updateBannerViewStatus()
                              }))
            
            RootViewManager.shared.alertContent = alert
            RootViewManager.shared.showAlert = true
            RootViewManager.shared.showAlertTrigger += 1

        } else {
            //購入履歴なし
            //購入手続き
            IAPMan.buy(productIdentifier: productIdentifier)
        }

    }
    //1度もアイテム購入したことがなく、リストアを実行した時
    func iapManagerDidFailedRestoreNeverPurchase() {
        //購入履歴なし
        //購入手続き
        IAPMan.buy(productIdentifier: productIdentifier)
    }
    //リストアに失敗した時
    func iapManagerDidFailedRestore() {
        //リストア失敗をユーザに知らせるアラートを表示
        self.showFailedDialog()
    }
    //特殊な購入時の延期の時
    func iapManagerDidDeferredPurchased() {
        //購入失敗をユーザに知らせるアラートを表示
        self.showFailedDialog()
    }

    func showFailedDialog() {
        
        let alert = Alert(title: Text(NSLocalizedString("loc_AlertAction_PurchaseFailed_Title", comment: "")),
                          message: Text(NSLocalizedString("loc_AlertAction_PurchaseFailed_Message", comment: "")),
                          dismissButton: .default(Text(NSLocalizedString("loc_OK", comment: "")), action: {
                            RootViewManager.shared.showProgress = false
                          }))
        
        RootViewManager.shared.alertContent = alert
        RootViewManager.shared.showAlert = true
        RootViewManager.shared.showAlertTrigger += 1

    }







    // Ad Interstatial

    func admob_CreateInterstitial() {

        if self.admob_interstitial == nil {
            self.admob_CreateInterstitialCore()
        } else {
            if self.admob_interstitial.isReady {

            } else {
                self.admob_CreateInterstitialCore()
            }
        }

    }

    func admob_CreateInterstitialCore() {

        self.admob_interstitial = GADInterstitial(adUnitID: self.AdMob_intersUnitID)
        self.admob_interstitial.load(GADRequest())

    }

    func showInterstitial() {

        let root = UIApplication.shared.windows.first?.rootViewController

        //Interstitialを表示しない
        if (self.isPurchased || self.isTrialMode || self.adProvider==0 ) {
            return
        }

        //Interstitialを表示する
        if adProvider == 2 {
            
            ADF_interstitial!.play(withPresenting: root)

        } else {
            //admob
            if self.admob_interstitial != nil {
                if self.admob_interstitial.isReady {
                    self.admob_interstitial.present(fromRootViewController: root!)
                    admob_CreateInterstitialCore()
                } else {
                    print("Ad wasn't ready")
                }
            }
        }

    }
    
}




