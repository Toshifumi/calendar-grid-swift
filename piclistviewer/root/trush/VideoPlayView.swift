//
//  VideoPlayView.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2021/01/08.
//

import Foundation
import UIKit
import Photos
import SwiftUI

struct VideoPlayView: View {
    
    var dismissAction: (() -> Void)
    var localIdentifier:String
    
    //    @Environment(\.isEnabled) private var isEnabled
    
    
    var body: some View {
        
        ZStack {
            
            PlayerView(localIdentifier: localIdentifier)
            
            VStack {
                //header for close icon
                Spacer()
                    .frame(height: 16)

                HStack {
                    Spacer()
                    Button(action: dismissAction ) {
                        Image(uiImage: UIImage(imageLiteralResourceName: "closeView"))
                            .resizable()
                            .scaledToFill()
                            .frame(width: 36, height: 36)
                    }

                    Spacer()
                        .frame(width: 16)
                }
                
                Spacer()

            }
        }
    }
    
}

struct PlayerView: UIViewRepresentable {
    
    var localIdentifier:String!

    
    func updateUIView(_ uiView: UIView, context: UIViewRepresentableContext<PlayerView>) {
    }
    
    func makeUIView(context: Context) -> UIView {
        return PlayerUIView(localIdentifier: self.localIdentifier)
    }
    
    
    class PlayerUIView: UIView {
        
        let localIdentifier:String!

        var playerLayer = AVPlayerLayer()
        
        var PlayButton:UIButton!
        
        
        init(localIdentifier:String) {

            self.localIdentifier = localIdentifier
            super.init(frame: .zero)
            
            self.loadVideo()

        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            playerLayer.frame = bounds

            if PlayButton != nil {
                PlayButton.removeFromSuperview()
            }
            PlayButton = UIButton(frame: CGRect(x: 16, y: bounds.height - 16 - 60, width: 60, height: 60))
            PlayButton.setImage(UIImage(named: "pauseVideoButton"), for: .normal)
            PlayButton.addTarget(self, action: #selector(buttonEvent), for: .touchUpInside)
            addSubview(PlayButton)

        }
        
        
        
        
        func loadVideo() {
            
            let results = PHAsset.fetchAssets(withLocalIdentifiers: [self.localIdentifier], options: nil)
            if ( results.count != 0  ) {
                //存在した
                let phasset = results.firstObject
                let options = PHImageRequestOptions()
                options.deliveryMode = .highQualityFormat
                
                //動画の場合、プレイヤーにロード
                if phasset?.mediaType == PHAssetMediaType.video {
                    //動画
                    PHImageManager.default().requestPlayerItem(forVideo: phasset!, options: nil) { (video, info) in
                        DispatchQueue.main.async { [self] in
                            if video != nil {
                                
                                let player = AVPlayer.init(playerItem: video)
                                let playerLayer = AVPlayerLayer(player: player)
                                playerLayer.videoGravity = .resizeAspect
                                self.layer.addSublayer(playerLayer)
                                self.playerLayer = playerLayer
                                let startButton = UIButton()
                                startButton.backgroundColor = UIColor.green
                                
                                // ボタンを押した時に実行するメソッドを指定
                                startButton.addTarget(self, action: #selector(self.buttonEvent(_:)), for: UIControl.Event.touchUpInside)

                                // 位置を決める(画面中央)
                                startButton.center = self.center
                                
                                self.addSubview(startButton)
                                
                                
                                NotificationCenter.default.addObserver(self, selector:#selector(self.playerDidFinishPlaying(note:)),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerLayer.player?.currentItem)
                                player.play()

                            }
                        }
                    }
                }
                    
            } else {
                //存在しない
            }
        }
        
        
        // ボタンが押された時に呼ばれるメソッド
        @objc func buttonEvent(_ sender: UIButton) {
            print("ボタンの情報: \(sender)")
            
            
            if playerLayer.player?.timeControlStatus == AVPlayer.TimeControlStatus.playing  {
                //演奏中 -> 停止する
                PlayButton.setImage(UIImage(named: "playVideoButton"), for: .normal)
                playerLayer.player?.pause()
//                    playerLayer.player?.removeObserver(self, forKeyPath: "rate")
                NotificationCenter.default.removeObserver(self)
                
            } else {
                //停止中 ー＞再生する
                PlayButton.setImage(UIImage(named: "pauseVideoButton"), for: .normal)

                //                if PlayerLayer.player?.rate == 1 {
                //                    PlayerLayer.player?.seek(to: .zero)
                //                }
                playerLayer.player?.play()
//                    playerLayer.player?.addObserver(self, forKeyPath: "rate", options: [], context: nil)
                NotificationCenter.default.addObserver(self, selector:#selector(self.playerDidFinishPlaying(note:)),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerLayer.player?.currentItem)
                
            }
        }
        
        
        @objc func playerDidFinishPlaying(note: NSNotification){
            //Called when player finished playing
            playerLayer.player?.seek(to: .zero)
            PlayButton.setImage(UIImage(named: "playVideoButton"), for: .normal)
//            playerLayer.player?.removeObserver(self, forKeyPath: "rate")
            NotificationCenter.default.removeObserver(self)
        }
        
        override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
            //.AVPlayerからのmessageハンドリングのため必要
        }
    }
}





struct VideoPlayView_Previews: PreviewProvider {
    static var previews: some View {
        VideoPlayView(dismissAction: { UIViewController().dismiss( animated: true, completion: nil )}, localIdentifier: "")
    }
}
