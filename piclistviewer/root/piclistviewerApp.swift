//
//  piclistviewerApp.swift
//  piclistviewer
//
//  Created by 堂田敏文 on 2020/12/15.
//

import SwiftUI
import Firebase
import AdSupport
import AppTrackingTransparency

@main
struct piclistviewerApp: App {
    
//    @EnvironmentObject var envData:EnvData

    init(){
        //起動時処理はここ
        requestIDFA()
        
        FirebaseApp.configure()

        _ = AssetManager.shared
        _ = LicenseManager.shared
        _ = RootViewManager.shared
        
    }
    
    var body: some Scene {
        WindowGroup {

            MainView()
//                .environmentObject(EnvData())
            
        }
        
    }
    
    func requestIDFA() {
        
        if #available(iOS 14, *) {
            ATTrackingManager.requestTrackingAuthorization { status in
                EventManager.shared.callAnswer_IDFAHasBeenShown(isCompleted: true)
                   switch status {
                   case .authorized:
                       // Tracking authorization dialog was shown
                       // and we are authorized
                       print("Authorized")
                   
                       // Now that we are authorized we can get the IDFA
                   print(ASIdentifierManager.shared().advertisingIdentifier)
                   case .denied:
                      // Tracking authorization dialog was
                      // shown and permission is denied
                        print("Denied")
                   case .notDetermined:
                           // Tracking authorization dialog has not been shown
                           print("Not Determined")
                   case .restricted:
                           print("Restricted")
                   @unknown default:
                           print("Unknown")
                   }
               }
            

        }

    }
    
}
