#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "MovieInterstitial6002.h"
#import "MovieReward6002.h"
#import "Banner6019.h"
#import "MovieInterstitial6019.h"
#import "MovieNative6019.h"
#import "MovieReward6019.h"
#import "Rectangle6019.h"
#import "Banner6000.h"
#import "MovieInterstitial6000.h"
#import "MovieNative6000.h"
#import "MovieReward6000.h"
#import "Banner6016.h"
#import "MovieInterstitial6016.h"
#import "MovieNative6016.h"
#import "MovieReward6016.h"
#import "Rectangle6016.h"
#import "MovieInterstitial6004.h"
#import "MovieReward6004.h"
#import "Banner6020.h"
#import "MovieInterstitial6020.h"
#import "MovieNative6020.h"
#import "MovieReward6020.h"
#import "Rectangle6020.h"
#import "MovieInterstitial6009.h"
#import "MovieNative6009.h"
#import "MovieReward6009.h"
#import "MovieInterstitial6017.h"
#import "MovieNative6017.h"
#import "MovieReward6017.h"
#import "MovieInterstitial6005.h"
#import "MovieReward6005.h"
#import "Banner6001.h"
#import "MovieInterstitial6001.h"
#import "MovieReward6001.h"
#import "MovieInterstitial6006.h"
#import "MovieNativeAdFlex6006.h"
#import "MovieReward6006.h"

FOUNDATION_EXPORT double AdfurikunSDK_iOSVersionNumber;
FOUNDATION_EXPORT const unsigned char AdfurikunSDK_iOSVersionString[];

