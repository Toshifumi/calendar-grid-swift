
//
//  MovieReward6001.m(UnityAds)
//
//  Copyright (c) A .D F. U. L. L. Y Co., Ltd. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import "MoPub.h"
#import "MPInterstitialAdControllerDelegate.h"
#import <ADFMovieReward/ADFmyMovieRewardInterface.h>
#import "MPInterstitialAdController.h"

@interface MovieInterstitial6020 : ADFmyMovieRewardInterface<MPInterstitialAdControllerDelegate>

@end
