//
//  MovieNative6017.h
//  MovieRewardTestApp
//
//  Created by Sungil Kim on 2020/10/23.
//  Copyright © 2020 Glossom, Inc. All rights reserved.
//

#import <ADFMovieReward/ADFMovieReward.h>
#import <BUAdSDK/BUAdSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface MovieNative6017 : ADFmyMovieNativeInterface <BUNativeAdDelegate, BUVideoAdViewDelegate>

@end

@interface MovieNativeAdInfo6017 : ADFMovieNativeAdInfo

@end

NS_ASSUME_NONNULL_END
