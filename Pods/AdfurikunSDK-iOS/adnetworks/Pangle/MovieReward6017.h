//
//  MovieReward6017.h
//  MovieRewardTestApp
//
//  Created by Ren Fujii on 2019/08/20.
//  Copyright © 2019 A .D F. U. L. L. Y Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ADFMovieReward/ADFmyMovieRewardInterface.h>

NS_ASSUME_NONNULL_BEGIN

@interface MovieReward6017 : ADFmyMovieRewardInterface

@end

NS_ASSUME_NONNULL_END
